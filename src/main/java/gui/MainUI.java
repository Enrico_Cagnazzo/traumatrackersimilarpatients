package gui;

import inputElaboration.Database;
import inputElaboration.NewPatient;
import similarPatientsCalculation.CalculateSimilarPatients;
import similarPatientsCalculation.EuclideanMetric;
import similarPatientsCalculation.NormalizationEnum;
import similarPatientsCalculation.UnifiedMetricCheungJia;
import similarPatientsCalculation.PatientAndDistance;
import utilities.StatisticUtilities;
import utilities.exceptions.InvalidNewPatientSourceException;
import utilities.exceptions.InvalidReportFileException;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Objects;

public class MainUI extends JFrame{

    private static final String ERROR_MESSAGE = "Error";
    private static final int NUMBER_OF_PATIENTS_TO_SHOW=5;

    private JPanel inputPanel;
    private JPanel outputPanel;
    private JComboBox<String> metricComboBox;
    private JComboBox<String> normalizationComboBox;
    private JTextField newPatientSource;

    public MainUI() {

        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException |
                    InstantiationException |
                    IllegalAccessException |
                    UnsupportedLookAndFeelException ex) {
                ex.printStackTrace();
            }

            setLayout(new BorderLayout());

            JLabel welcomeLabel=new JLabel("Trauma Tracker Similar Patients");
            welcomeLabel.setFont(new Font("Verdana", Font.BOLD, 20));
            welcomeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

            this.inputPanel = createInputPanel();
            this.outputPanel = new JPanel();

            add(welcomeLabel,BorderLayout.NORTH);
            add(inputPanel,BorderLayout.WEST);

            this.setVisible(true);
            this.setResizable(true);
            this.pack();

            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        });
    }



    /**
     * Shows an error dialog for the wrong input
     * @param errorType a brief description of the error
     */
    private void showErrorDialog(String errorType) {
        JOptionPane.showMessageDialog(inputPanel,
                errorType, ERROR_MESSAGE,
                JOptionPane.ERROR_MESSAGE);
    }

    private JPanel createInputPanel(){
        JPanel input=new JPanel();
        input.setLayout(new BoxLayout(input,BoxLayout.Y_AXIS));
        JPanel database=new JPanel();
        JPanel newPatient=new JPanel();
        JPanel metric=new JPanel();
        JLabel newPatientLabel=new JLabel("New patient source: ", JLabel.LEFT);
        newPatientSource=new JTextField(30);
        JButton chooseNewPatientSourceButton=new JButton("Choose ...");
        chooseNewPatientSourceButton.addActionListener(e -> chooseNewPatientSource());
        JLabel metricLabel=new JLabel("Metric: ", JLabel.LEFT);
        JLabel normalizationLabel=new JLabel("Normalization: ", JLabel.LEFT);
        metricComboBox=new JComboBox<>();
        metricComboBox.addItem(EuclideanMetric.name);
        metricComboBox.addItem(UnifiedMetricCheungJia.name);
        normalizationComboBox=new JComboBox<>();
        for (NormalizationEnum normalization: NormalizationEnum.values()){
            normalizationComboBox.addItem(normalization.toString());
        }
        JButton calculateButton=new JButton("Calculate");
        calculateButton.addActionListener(e->calculateSimilarPatients());
        input.add(database);
        newPatient.add(newPatientLabel);
        newPatient.add(newPatientSource);
        newPatient.add(chooseNewPatientSourceButton);
        input.add(newPatient);
        metric.add(metricLabel);
        metric.add(metricComboBox);
        metric.add(normalizationLabel);
        metric.add(normalizationComboBox);
        input.add(metric);
        input.add(calculateButton,BorderLayout.EAST);
        return input;
    }

    private void calculateSimilarPatients() {
        EventQueue.invokeLater(()->{
            try {
                NormalizationEnum normalization=NormalizationEnum.MAX_MIN;
                for (NormalizationEnum normalizationEnum:NormalizationEnum.values()){
                    if (normalizationEnum.toString().equals(normalizationComboBox.getSelectedItem())){
                        normalization=normalizationEnum;
                        break;
                    }
                }
                switch (Objects.requireNonNull(metricComboBox.getSelectedItem()).toString()){
                    case EuclideanMetric.name:
                        CalculateSimilarPatients.setParameters(normalization, new EuclideanMetric());
                        break;
                    case UnifiedMetricCheungJia.name:
                        CalculateSimilarPatients.setParameters(normalization, new UnifiedMetricCheungJia());
                        break;
                }

                Database.create();
                NewPatient.setPath(newPatientSource.getText());
                StatisticUtilities.computeNormalizationFactors();

                Iterable<PatientAndDistance> results= CalculateSimilarPatients.calculateSimilar();

                remove(outputPanel);
                this.outputPanel=showResults(results);
                add(outputPanel,BorderLayout.EAST);
                this.repaint();
                this.revalidate();
                this.pack();
            } catch (InvalidNewPatientSourceException e){
                showErrorDialog(e.toString());
            } catch (InvalidReportFileException | IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void chooseNewPatientSource(){
        JFileChooser fileChooser = new JFileChooser(System.getProperty("user.home"));
        fileChooser.setAcceptAllFileFilterUsed(true);
        int returnVal = fileChooser.showOpenDialog(new JFrame());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            this.newPatientSource.setText(fileChooser.getSelectedFile().getAbsolutePath());
        }
    }


    private JPanel showResults(Iterable<PatientAndDistance> results) {
        JPanel output=new JPanel();
        output.setLayout(new BoxLayout(output,BoxLayout.Y_AXIS));
        int i=0;
        for (PatientAndDistance patientAndDistance:results){
            JPanel patient=new JPanel();
            JLabel patientID=new JLabel(patientAndDistance.getDistance()+" "+patientAndDistance.getPatient().getId());
            patient.add(patientID);
            output.add(patientID);
            if (++i==NUMBER_OF_PATIENTS_TO_SHOW)
                break;
        }
        return output;
    }
}

