import shockPredictor.RNN.LstmRnn;

public class StartShockPredictor {

    public static void main(String[] args) {

        LstmRnn lstmRnn= LstmRnn.getInstance();
        System.out.println("rete inizializzata");

        lstmRnn.train();

        lstmRnn.validate();
    }
}
