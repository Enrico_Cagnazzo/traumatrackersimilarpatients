package shockPredictor.RNN;

import inputElaboration.Database;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.distribution.UniformDistribution;
import org.deeplearning4j.nn.conf.layers.LSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.jetbrains.annotations.NotNull;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.cpu.nativecpu.NDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;
import report.Event;
import report.Patient;
import shockPredictor.EventRNN;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class LstmRnn {
    private final static double LEARNING_RATE=Adam.DEFAULT_ADAM_LEARNING_RATE; //Valore suggerito nella presentazione 0.1 "Instantaneous Heart Rate ..."
    private final static int LSTM_LAYER_SIZE=50;  //stessa proporzione di "dynamic mortality ..." 2/3 dei neuroni di input

    private final static double PATIENTS_FOR_LEARNING=0.8;

    private final static int MINUTES_BEFORE_SHOCK=15;

    private final static int NOT_SHOCKED_CLASS=0;
    private final static int SHOCKED_CLASS=1;

    private MultiLayerNetwork net;
    private Queue<Patient> database;

    private static LstmRnn ourInstance = new LstmRnn();
    private static int nInput=0;

    public static LstmRnn getInstance() {
        return ourInstance;
    }

    private LstmRnn() {
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .weightInit(new UniformDistribution(-1/Math.sqrt(EventRNN.getNumberOfFields()),1/Math.sqrt(EventRNN.getNumberOfFields())))
                .updater(new Adam(LEARNING_RATE))
                .list()
                .layer(0, new LSTM.Builder().nIn(EventRNN.getNumberOfFields()).nOut(LSTM_LAYER_SIZE)
                        .activation(Activation.SOFTSIGN).build()) //come suggerito in https://deeplearning4j.org/lstm#
                .layer(1,new LSTM.Builder().nIn(LSTM_LAYER_SIZE).nOut(LSTM_LAYER_SIZE)
                        .activation(Activation.SOFTSIGN).build())
                .layer(2,new RnnOutputLayer.Builder(LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nIn(LSTM_LAYER_SIZE).nOut(2).activation(Activation.SOFTMAX).build())
                .pretrain(false).backprop(true).build();

        this.net=new MultiLayerNetwork(conf);
        this.net.init();
        this.net.setListeners(new ScoreIterationListener(10));
                            //i risultati dell'allenamento vengono stampati ad ogni iterazione

        System.out.println("neuroni di input: "+EventRNN.getNumberOfFields());
        Layer[] layers = net.getLayers();
        int totalNumParams = 0;
        for( int i=0; i<layers.length; i++ ){
            int nParams = layers[i].numParams();
            System.out.println("Number of parameters in layer " + i + ": " + nParams);
            totalNumParams += nParams;
        }
        System.out.println("Total number of network parameters: " + totalNumParams);

    }

    public void train() {
        this.database=new LinkedBlockingQueue<>(Database.getInstance().getDatabase());
        double nPatients=this.database.size()*PATIENTS_FOR_LEARNING;
        int skipped=0;
        for (int i=0;i<nPatients;i++){
            Patient actualPatient=this.database.poll();
            if (actualPatient==null || actualPatient.getEventsForRNN().isEmpty()){
                skipped++;
                continue;
            }
            nInput+=actualPatient.getEventsForRNN().size();
            this.net.fit(getPatientDataset(actualPatient));
        }
        System.out.flush();

        System.out.println("Sono stati saltati "+skipped+" pazienti");

        System.out.println("rete allenata con "+nInput+ " input. Num eventi medio per paziente: "+(double)nInput/(nPatients/PATIENTS_FOR_LEARNING));

        System.out.println("rimangono "+this.database.size()+" pazienti, n totale: "+nPatients/PATIENTS_FOR_LEARNING);
    }

    public void validate() {
        Evaluation eval = new Evaluation(2);
        int skipped=0;
        while(!this.database.isEmpty()){
            Patient actualPatient=this.database.poll();
            if (actualPatient==null || actualPatient.getEventsForRNN().isEmpty()) {
                skipped++;
                continue;
            }
            DataSet dataSet = getPatientDataset(actualPatient);
            INDArray features = dataSet.getFeatureMatrix();
            INDArray output = dataSet.getLabels();
            INDArray predicted = this.net.output(features,false);

            eval.eval(output, predicted);
        }

        System.out.println("Sono stati saltati "+skipped+" pazienti");


        System.out.println("rete validata. \nI risultati sono i seguenti:");
        //Print the evaluation statistics
        System.out.println(eval.stats());
    }

    @NotNull
    private DataSet getPatientDataset(Patient patient) {
        List<EventRNN> events=new ArrayList<>(patient.getEventsForRNN());
        Event shockEvent=patient.getEventShock();
        double features[][]=new double[events.size()][EventRNN.getNumberOfFields()];
        double labels[][]=new double[events.size()][2];
        for (int i =0;i<events.size();i++){
            features[i]=events.get(i).getFeatures();
            labels[i]=calculateOutput(shockEvent,events.get(i));
        }
        return new DataSet(new NDArray(features), new NDArray(labels));
    }

    private double[] calculateOutput(Event shockEvent, EventRNN eventRNN) {
        double results[]= new double[2];
        if (shockEvent==null ||
                shockEvent.getDateAndTime().getTime()-eventRNN.getDate().getTime()>MINUTES_BEFORE_SHOCK*60*1000){
            results[NOT_SHOCKED_CLASS]=1; results[SHOCKED_CLASS]=0;
            return results;
        }
        results[NOT_SHOCKED_CLASS]=0; results[SHOCKED_CLASS]=1;
        return results;
    }
}
