package shockPredictor;


import shockPredictor.diagnostic.DiagnosticRNN;
import shockPredictor.patientState.PatientStateRNN;
import initialState.InitialState;
import report.Event;

import java.util.Date;
import java.util.Objects;

public class EventRNN {
    private int id;
    private Date date;
    private ProcedureRNN procedureRNN;
    private DiagnosticRNN diagnosticRNN;
    private PatientStateRNN patientState;
    private DrugRNN drugRNN;

    public EventRNN(Event event, InitialState initialState) {
        this.id=event.getEventID();
        this.date=event.getDateAndTime();

        this.procedureRNN=new ProcedureRNN();
        this.diagnosticRNN=new DiagnosticRNN();
        this.patientState=new PatientStateRNN(initialState);
        this.drugRNN=new DrugRNN();
        setNewValues(event);
    }

    public EventRNN(EventRNN eventPrev, Event event) {
        this.id=event.getEventID();
        this.date=event.getDateAndTime();

        this.procedureRNN=new ProcedureRNN();
        this.diagnosticRNN=new DiagnosticRNN(eventPrev.getDiagnosticRNN());
        this.drugRNN=new DrugRNN(eventPrev.getDrugRNN());
        this.patientState=new PatientStateRNN(eventPrev.getPatientState());

        setNewValues(event);
    }

    public static int getNumberOfFields() {
        return ProcedureRNN.getNumberOfFields()+DiagnosticRNN.getNumberOfFields()
                +PatientStateRNN.getNumberOfFields()+DrugRNN.getNumberOfFields();
    }

    private void setNewValues(Event event){
        switch (event.getType()){
            case "procedure":
                this.procedureRNN=new ProcedureRNN(event);
                break;
            case "diagnostic":
                this.diagnosticRNN.setNewValues(event);
                break;
            case "drug":
                this.drugRNN.setNewValues(event);
                break;
            case "vital-signs-mon":
                this.patientState.updateMon(event);
                break;
            case "vital-sign":
                this.patientState.updateValue(event);
                break;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventRNN)) return false;
        EventRNN eventRNN = (EventRNN) o;
        return Objects.equals(procedureRNN, new ProcedureRNN()) &&
                Objects.equals(eventRNN.procedureRNN, new ProcedureRNN()) &&
                Objects.equals(diagnosticRNN, new DiagnosticRNN()) &&
                Objects.equals(eventRNN.diagnosticRNN, new DiagnosticRNN()) &&
                Objects.equals(patientState, eventRNN.getPatientState()) &&
                Objects.equals(drugRNN, eventRNN.getDrugRNN());
    }


    public PatientStateRNN getPatientState() {
        return patientState;
    }

    public DrugRNN getDrugRNN() {
        return drugRNN;
    }

    public DiagnosticRNN getDiagnosticRNN() {
        return diagnosticRNN;
    }

    public Date getDate(){
        return this.date;
    }

    public double[] getFeatures() {
        double fields[]= new double[EventRNN.getNumberOfFields()];
        int pos=0;
        double patientStateArray[]=this.patientState.getFields();
        double procedureArray[]=this.procedureRNN.getFields();
        double diagnosticArray[]=this.diagnosticRNN.getFields();
        double drugArray[]=this.drugRNN.getFields();
        System.arraycopy(patientStateArray, 0, fields, pos, patientStateArray.length);
        pos+=patientStateArray.length;
        System.arraycopy(procedureArray, 0, fields, pos, procedureArray.length);
        pos+=procedureArray.length;
        System.arraycopy(diagnosticArray, 0, fields, pos, diagnosticArray.length);
        pos+=diagnosticArray.length;
        System.arraycopy(drugArray, 0, fields, pos, drugArray.length);
        return fields;
    }
}
