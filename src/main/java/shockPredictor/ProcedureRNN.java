package shockPredictor;

import report.Event;
import utilities.RNNUtilities;

import java.util.Objects;

public class ProcedureRNN {
    private String id;

    public ProcedureRNN(Event event) {
        this.id = event.getContent().getProcedureId();
    }

    public ProcedureRNN() {

    }

    public static int getNumberOfFields() {
        return 1;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProcedureRNN)) return false;
        ProcedureRNN that = (ProcedureRNN) o;
        return Objects.equals(getId(), that.getId());
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        fields[0]=RNNUtilities.getHashDoubleFromString(this.id);
        return fields;
    }
}
