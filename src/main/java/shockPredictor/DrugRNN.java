package shockPredictor;

import report.Event;
import report.EventContent;
import utilities.RNNUtilities;

import java.util.Objects;

public class DrugRNN {
    private double adrenalineContinuousInf;
    private double noradrenalineContinuousInf;
    private double dopamineContinuousInf;
    private double fentanilContinuousInf;
    private double propofolContinuousInf;
    private double ketanestContinuousInf;
    private double midazolamContinuousInf;
    private double thiopentalContinuousInf;
    private double genericDrugContinuousInf;

    private String oneShotDrugId = "";
    private double oneShotDrugQty = 0;

    public DrugRNN() {
        this.adrenalineContinuousInf = 0;
        this.noradrenalineContinuousInf = 0;
        this.dopamineContinuousInf = 0;
        this.fentanilContinuousInf = 0;
        this.propofolContinuousInf = 0;
        this.ketanestContinuousInf = 0;
        this.midazolamContinuousInf = 0;
        this.thiopentalContinuousInf = 0;
        this.genericDrugContinuousInf = 0;
    }

    public DrugRNN(DrugRNN previous) {
        this.adrenalineContinuousInf = previous.adrenalineContinuousInf;
        this.noradrenalineContinuousInf = previous.noradrenalineContinuousInf;
        this.dopamineContinuousInf = previous.dopamineContinuousInf;
        this.fentanilContinuousInf = previous.fentanilContinuousInf;
        this.propofolContinuousInf = previous.propofolContinuousInf;
        this.ketanestContinuousInf = previous.ketanestContinuousInf;
        this.midazolamContinuousInf = previous.midazolamContinuousInf;
        this.thiopentalContinuousInf = previous.thiopentalContinuousInf;
        this.genericDrugContinuousInf = previous.genericDrugContinuousInf;
        this.oneShotDrugId="";
        this.oneShotDrugQty=0;
    }

    public static int getNumberOfFields() {
        return 11;
    }

    public void setNewValues(Event event) {
        EventContent content = event.getContent();
        if (content.getAdministrationType() == null || content.getAdministrationType().equals("one-shot")) {
            oneShotDrugId = content.getDrugId();
            oneShotDrugQty = content.getQty();
            return;
        }
        if (content.getAdministrationType().equals("drug-protocol")) {
            oneShotDrugId = content.getDrugId();
            oneShotDrugQty = 1;
            return;
        }
        switch (content.getDrugId()) {
            case "adrenaline-continuous-inf":
                adrenalineContinuousInf = newQuantity(content);
                break;
            case "noradrenaline-continuous-inf":
                noradrenalineContinuousInf = newQuantity(content);
                break;
            case "dopamine-continuous-inf":
                dopamineContinuousInf = newQuantity(content);
                break;
            case "fentanil-continuous-inf":
                fentanilContinuousInf = newQuantity(content);
                break;
            case "propofol-continuous-inf":
                propofolContinuousInf = newQuantity(content);
                break;
            case "ketanest-continuous-inf":
                ketanestContinuousInf = newQuantity(content);
                break;
            case "midazolam-continuous-inf":
                midazolamContinuousInf = newQuantity(content);
                break;
            case "thiopental-continuous-inf":
                thiopentalContinuousInf = newQuantity(content);
                break;
            case "generic-drug-continuous-inf":
                genericDrugContinuousInf = newQuantity(content);
                break;
        }
    }

    private double newQuantity(EventContent content) {
        if (content.getEvent().equals("stop"))
            return 0;
        return content.getQty();
    }


    public double getAdrenalineContinuousInf() {
        return adrenalineContinuousInf;
    }

    public double getNoradrenalineContinuousInf() {
        return noradrenalineContinuousInf;
    }

    public double getDopamineContinuousInf() {
        return dopamineContinuousInf;
    }

    public double getFentanilContinuousInf() {
        return fentanilContinuousInf;
    }

    public double getPropofolContinuousInf() {
        return propofolContinuousInf;
    }

    public double getKetanestContinuousInf() {
        return ketanestContinuousInf;
    }

    public double getMidazolamContinuousInf() {
        return midazolamContinuousInf;
    }

    public double getThiopentalContinuousInf() {
        return thiopentalContinuousInf;
    }

    public double getGenericDrugContinuousInf() {
        return genericDrugContinuousInf;
    }

    public String getOneShotDrugId() {
        return oneShotDrugId;
    }

    public double getOneShotDrugQty() {
        return oneShotDrugQty;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DrugRNN)) return false;
        DrugRNN drugRNN = (DrugRNN) o;
        return Double.compare(drugRNN.getAdrenalineContinuousInf(), getAdrenalineContinuousInf()) == 0 &&
                Double.compare(drugRNN.getNoradrenalineContinuousInf(), getNoradrenalineContinuousInf()) == 0 &&
                Double.compare(drugRNN.getDopamineContinuousInf(), getDopamineContinuousInf()) == 0 &&
                Double.compare(drugRNN.getFentanilContinuousInf(), getFentanilContinuousInf()) == 0 &&
                Double.compare(drugRNN.getPropofolContinuousInf(), getPropofolContinuousInf()) == 0 &&
                Double.compare(drugRNN.getKetanestContinuousInf(), getKetanestContinuousInf()) == 0 &&
                Double.compare(drugRNN.getMidazolamContinuousInf(), getMidazolamContinuousInf()) == 0 &&
                Double.compare(drugRNN.getThiopentalContinuousInf(), getThiopentalContinuousInf()) == 0 &&
                Double.compare(drugRNN.getGenericDrugContinuousInf(), getGenericDrugContinuousInf()) == 0 &&
                Double.compare(drugRNN.getOneShotDrugQty(), getOneShotDrugQty()) == 0 &&
                Objects.equals(getOneShotDrugId(), drugRNN.getOneShotDrugId());
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        fields[0]=adrenalineContinuousInf;
        fields[1]=noradrenalineContinuousInf;
        fields[2]=dopamineContinuousInf;
        fields[3]=fentanilContinuousInf;
        fields[4]=propofolContinuousInf;
        fields[5]=ketanestContinuousInf;
        fields[6]=midazolamContinuousInf;
        fields[7]=thiopentalContinuousInf;
        fields[8]=genericDrugContinuousInf;
        fields[9]=RNNUtilities.getHashDoubleFromString(oneShotDrugId);
        fields[10]=oneShotDrugQty;
        return fields;
    }
}
