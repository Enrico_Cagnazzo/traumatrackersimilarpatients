package shockPredictor.diagnostic;

import report.Event;
import report.EventContent;
import utilities.RNNUtilities;
import utilities.StringUtilities;

import java.util.Objects;

public class RotemResults {
    public static final String DIAGNOSTIC_ID="rotem";

    private RotemTestResults fibtem;
    private RotemTestResults extem;
    private Boolean hyperfibrinolysis;

    public RotemResults(Event event) {
        EventContent content=event.getContent();
        this.fibtem = new RotemTestResults(content.getFibtem());
        this.extem = new RotemTestResults(content.getExtem());
        if (!StringUtilities.isNullOrEmpty(content.getHyperfibrinolysis()))
            this.hyperfibrinolysis = content.getHyperfibrinolysis().equals("si");
    }

    public RotemResults() {
        this.fibtem=new RotemTestResults();
        this.extem=new RotemTestResults();
    }

    public RotemResults(RotemResults previousRotem) {
        this.fibtem = new RotemTestResults(previousRotem.getFibtem());
        this.extem = new RotemTestResults(previousRotem.getExtem());
        this.hyperfibrinolysis=previousRotem.hyperfibrinolysis;
    }

    public static int getNumberOfFields() {
        return 2*RotemTestResults.getNumberOfFields()+1;
    }

    public RotemTestResults getExtem() {
        return extem;
    }

    public Boolean getHyperfibrinolysis() {
        return hyperfibrinolysis;
    }

    public RotemTestResults getFibtem() {
        return fibtem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RotemResults)) return false;
        RotemResults that = (RotemResults) o;
        return Objects.equals(getFibtem(), that.getFibtem()) &&
                Objects.equals(getExtem(), that.getExtem()) &&
                Objects.equals(getHyperfibrinolysis(), that.getHyperfibrinolysis());
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        int pos=0;

        if (this.fibtem==null)
            this.fibtem=new RotemTestResults();
        double fibtemArray[]=this.fibtem.getFields();
        if (this.extem==null)
            this.extem=new RotemTestResults();

        double extemArray[]=this.extem.getFields();
        System.arraycopy(fibtemArray, 0, fields, pos, fibtemArray.length);
        pos+=fibtemArray.length;
        System.arraycopy(extemArray, 0, fields, pos, extemArray.length);
        pos+=extemArray.length;
        fields[pos]= RNNUtilities.getDoubleFromBoolean(hyperfibrinolysis);
        return fields;
    }
}
