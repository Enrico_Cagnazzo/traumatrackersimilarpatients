package shockPredictor.diagnostic;

import report.Event;
import report.EventContent;
import utilities.RNNUtilities;

import java.util.Objects;

public class EgaResults {
    public static final String DIAGNOSTIC_ID="abg";

    private String lactates;
    private String be;
    private String ph;
    private String hb;
    private String glycemia;
    private String pco2;
    private String po2;

    public EgaResults(Event event){
        EventContent content=event.getContent();
        this.lactates=content.getLactates();
        this.be=content.getBe();
        this.ph=content.getPh();
        this.hb = content.getHb();
        this.glycemia = content.getGlycemia();
        this.pco2 = content.getPco2();
        this.po2 = content.getPo2();
    }

    public EgaResults() {
    }

    public EgaResults(EgaResults previousEga) {
        this.lactates=previousEga.getLactates();
        this.be=previousEga.getBe();
        this.ph=previousEga.getPh();
        this.hb = previousEga.getHb();
        this.glycemia = previousEga.getGlycemia();
        this.pco2 = previousEga.getPco2();
        this.po2 = previousEga.getPo2();
    }

    public static int getNumberOfFields() {
        return 7;
    }

    public String getLactates() {
        return lactates;
    }

    public String getBe() {
        return be;
    }

    public String getPh() {
        return ph;
    }

    public String getHb() {
        return hb;
    }

    public String getGlycemia() {
        return glycemia;
    }

    public String getPco2() {
        return pco2;
    }

    public String getPo2() {
        return po2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EgaResults)) return false;
        EgaResults that = (EgaResults) o;
        return Objects.equals(getLactates(), that.getLactates()) &&
                Objects.equals(getBe(), that.getBe()) &&
                Objects.equals(getPh(), that.getPh()) &&
                Objects.equals(getHb(), that.getHb()) &&
                Objects.equals(getGlycemia(), that.getGlycemia()) &&
                Objects.equals(getPco2(), that.getPco2()) &&
                Objects.equals(getPo2(), that.getPo2());
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        fields[0]=RNNUtilities.getDoubleFromString(this.lactates);
        fields[1]= RNNUtilities.getDoubleFromString(this.be);
        fields[2]= RNNUtilities.getDoubleFromString(this.ph);
        fields[3]= RNNUtilities.getDoubleFromString(this.hb);
        fields[4]= RNNUtilities.getDoubleFromString(this.glycemia);
        fields[5]= RNNUtilities.getDoubleFromString(this.pco2);
        fields[6]= RNNUtilities.getDoubleFromString(this.po2);
        return fields;
    }
}
