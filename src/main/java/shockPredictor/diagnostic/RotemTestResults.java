package shockPredictor.diagnostic;

import utilities.RNNUtilities;
import utilities.StringUtilities;

import java.util.Objects;

public class RotemTestResults {
    private Integer ct;
    private Integer cft;
    private Integer mcf;
    private Integer a5;
    private Integer a10;
    private Integer a15;
    private Integer a20;
    private Integer li30;
    private Integer alpha;

    public RotemTestResults(String results) {
        if (StringUtilities.isNullOrEmpty(results))
            return;
        results=results.replace(" = ", " ");
        results=results.replace(";", "");
        String values[]=results.split(" ");
        for (int i=0;i<values.length;i+=2){
            switch (values[i]){
                case "CT":
                    ct = Integer.parseInt(values[i + 1]);
                    break;
                case "CFT":
                    cft = Integer.parseInt(values[i + 1]);
                    break;
                case "MCF":
                    mcf = Integer.parseInt(values[i + 1]);
                    break;
                case "A5":
                    a5 = Integer.parseInt(values[i + 1]);
                    break;
                case "A10":
                    a10 = Integer.parseInt(values[i + 1]);
                    break;
                case "A15":
                    a15 = Integer.parseInt(values[i + 1]);
                    break;
                case "A20":
                    a20 = Integer.parseInt(values[i + 1]);
                    break;
                case "LI30":
                    li30 = Integer.parseInt(values[i + 1]);
                    break;
                case "alpha":
                    alpha=Integer.parseInt(values[i + 1]);
                    break;
            }
        }
    }

    public RotemTestResults() {

    }

    public RotemTestResults(RotemTestResults previousResults) {
        this.ct=previousResults.getCt();
        this.cft=previousResults.getCft();
        this.mcf=previousResults.getMcf();
        this.a5=previousResults.getA5();
        this.a10=previousResults.getA10();
        this.a15=previousResults.getA15();
        this.a20=previousResults.getA20();
        this.li30=previousResults.getLi30();
        this.alpha=previousResults.getAlpha();
    }


    public static int getNumberOfFields(){
        return 9;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RotemTestResults)) return false;
        RotemTestResults that = (RotemTestResults) o;
        return Objects.equals(alpha, that.alpha) &&
                Objects.equals(ct, that.ct) &&
                Objects.equals(cft, that.cft) &&
                Objects.equals(mcf, that.mcf) &&
                Objects.equals(a5, that.a5) &&
                Objects.equals(a10, that.a10) &&
                Objects.equals(a15, that.a15) &&
                Objects.equals(a20, that.a20) &&
                Objects.equals(li30, that.li30);
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        fields[0]=RNNUtilities.getDoubleFromInteger(ct);
        fields[1]=RNNUtilities.getDoubleFromInteger(cft);
        fields[2]=RNNUtilities.getDoubleFromInteger(mcf);
        fields[3]=RNNUtilities.getDoubleFromInteger(a5);
        fields[4]=RNNUtilities.getDoubleFromInteger(a10);
        fields[5]=RNNUtilities.getDoubleFromInteger(a15);
        fields[6]=RNNUtilities.getDoubleFromInteger(a20);
        fields[7]=RNNUtilities.getDoubleFromInteger(li30);
        fields[8]=RNNUtilities.getDoubleFromInteger(alpha);
        return fields;
    }

    public Integer getCt() {
        return ct;
    }

    public Integer getCft() {
        return cft;
    }

    public Integer getMcf() {
        return mcf;
    }

    public Integer getA5() {
        return a5;
    }

    public Integer getA10() {
        return a10;
    }

    public Integer getA15() {
        return a15;
    }

    public Integer getA20() {
        return a20;
    }

    public Integer getLi30() {
        return li30;
    }

    public Integer getAlpha() {
        return alpha;
    }
}
