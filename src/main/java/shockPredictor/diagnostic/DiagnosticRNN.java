package shockPredictor.diagnostic;

import report.Event;

import java.util.Objects;

public class DiagnosticRNN {
    private EgaResults egaResults;
    private RotemResults rotemResults;

    public DiagnosticRNN() {
        egaResults=new EgaResults();
        rotemResults=new RotemResults();
    }

    public DiagnosticRNN(DiagnosticRNN diagnosticRNN) {
        this.egaResults=new EgaResults(diagnosticRNN.getEgaResults());
        this.rotemResults=new RotemResults(diagnosticRNN.getRotemResults());
    }

    public void setNewValues(Event event) {
        switch (event.getContent().getDiagnosticId()){
            case EgaResults.DIAGNOSTIC_ID:
                egaResults=new EgaResults(event);
                break;
            case RotemResults.DIAGNOSTIC_ID:
                rotemResults=new RotemResults(event);
                break;
        }
    }

    public static int getNumberOfFields() {
        return EgaResults.getNumberOfFields()+RotemResults.getNumberOfFields();
    }

    public EgaResults getEgaResults() {
        return egaResults;
    }

    public RotemResults getRotemResults() {
        return rotemResults;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DiagnosticRNN)) return false;
        DiagnosticRNN that = (DiagnosticRNN) o;
        return Objects.equals(getEgaResults(), that.getEgaResults()) &&
                Objects.equals(getRotemResults(), that.getRotemResults());
    }


    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        if (this.egaResults==null)
            this.egaResults=new EgaResults();
        if (this.rotemResults==null)
            this.rotemResults=new RotemResults();
        double egaFields[]=this.egaResults.getFields();
        double rotemFields[]=this.rotemResults.getFields();
        System.arraycopy(egaFields, 0, fields, 0, egaFields.length);
        System.arraycopy(rotemFields, 0, fields, egaFields.length, rotemFields.length);
        return fields;
    }

}
