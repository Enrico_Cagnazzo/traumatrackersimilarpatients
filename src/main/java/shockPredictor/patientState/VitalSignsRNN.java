package shockPredictor.patientState;

import initialState.VitalSigns;
import report.Event;
import report.VitalSignsMon;
import utilities.RNNUtilities;

import java.util.Objects;

public class VitalSignsRNN {
    private Double temp;
    private Integer HR;
    private Integer DIA;
    private Integer SYS;
    private Integer SpO2;
    private Double EtCO2;
    private Boolean externalHemorages;

    public VitalSignsRNN(VitalSigns vitalSigns) {
        this.temp = vitalSigns.getTemperatureContinue();
        this.HR = vitalSigns.getHeartRateContinue();
        this.SYS = vitalSigns.getSystolicBloodPressureContinue();
        this.SpO2 = vitalSigns.getOxygenSaturationContinue();
        this.EtCO2 = vitalSigns.getEndTidalCO2Continue();
        this.externalHemorages=vitalSigns.getExternalHemorages();
    }

    public VitalSignsRNN(VitalSignsRNN vitalSigns) {
        this.temp=vitalSigns.getTemp();
        this.HR=vitalSigns.getHR();
        this.DIA=vitalSigns.getDIA();
        this.SYS=vitalSigns.getSYS();
        this.SpO2=vitalSigns.getSpO2();
        this.EtCO2=vitalSigns.getEtCO2();
        this.externalHemorages=vitalSigns.getExternalHemorages();
    }

    public static int getNumberOfFields() {
        return 7;
    }

    public void updateMon(Event event){
        VitalSignsMon content=event.getContent().getContent();
        this.temp = content.getTemp();
        this.HR   = content.getHR();
        this.DIA  = content.getDIA();
        this.SYS  = content.getSYS();
        this.SpO2 = content.getSpO2();
        this.EtCO2= content.getEtCO2();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VitalSignsRNN)) return false;
        VitalSignsRNN that = (VitalSignsRNN) o;
        return Objects.equals(getTemp(), that.getTemp()) &&
                Objects.equals(getHR(), that.getHR()) &&
                Objects.equals(getDIA(), that.getDIA()) &&
                Objects.equals(getSYS(), that.getSYS()) &&
                Objects.equals(getSpO2(), that.getSpO2()) &&
                Objects.equals(getEtCO2(), that.getEtCO2()) &&
                Objects.equals(getExternalHemorages(), that.getExternalHemorages());
    }

    public Double getTemp() {
        return temp;
    }

    public Integer getHR() {
        return HR;
    }

    public Integer getDIA() {
        return DIA;
    }

    public Integer getSYS() {
        return SYS;
    }

    public Integer getSpO2() {
        return SpO2;
    }

    public Double getEtCO2() {
        return EtCO2;
    }

    public Boolean getExternalHemorages() {
        return externalHemorages;
    }

    public void setTemp(String temp) {
        this.temp = Double.valueOf(temp);
    }

    public void setHR(String HR) {
        this.HR = Integer.valueOf(HR);
    }

    public void setDIA(String DIA) {
        this.DIA = Integer.valueOf(DIA);
    }

    public void setSYS(String SYS) {
        this.SYS = Integer.valueOf(SYS);
    }

    public void setSpO2(String spO2) {
        SpO2 = Integer.valueOf(spO2);
    }

    public void setEtCO2(String etCO2) {
        EtCO2 = Double.valueOf(etCO2);
    }

    public void setExternalHemorages(String externalHemorages){
        this.externalHemorages=externalHemorages.equals("Si");
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        fields[0]=RNNUtilities.getDoubleFromDouble(temp);
        fields[1]=RNNUtilities.getDoubleFromInteger(HR);
        fields[2]=RNNUtilities.getDoubleFromInteger(DIA);
        fields[3]=RNNUtilities.getDoubleFromInteger(SYS);
        fields[4]=RNNUtilities.getDoubleFromInteger(SpO2);
        fields[5]=RNNUtilities.getDoubleFromDouble(EtCO2);
        fields[6]=RNNUtilities.getDoubleFromBoolean(externalHemorages);
        return fields;
    }
}
