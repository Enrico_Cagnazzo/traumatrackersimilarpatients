package shockPredictor.patientState;

import initialState.InitialState;
import initialState.LimbsEnum;
import initialState.*;
import report.Event;

import java.util.Objects;

public class PatientStateRNN {

    private AirwayAndBreathing airwayAndBreathing;
    private NeurologicalExamination neurologicalExamination;
    private PeripheralWristsEvaluation peripheralWristsEvaluation;
    private VascularAccesses vascularAccesses;
    private VitalSignsRNN vitalSigns;

    public PatientStateRNN(InitialState initialState) {
        this.airwayAndBreathing=initialState.getAirwayAndBreathing();
        this.neurologicalExamination=initialState.getNeurologicalExamination();
        this.peripheralWristsEvaluation=initialState.getPeripheralWristsEvaluation();
        this.vascularAccesses=initialState.getVascularAccesses();
        this.vitalSigns=new VitalSignsRNN(initialState.getVitalSigns());
    }

    public PatientStateRNN(PatientStateRNN patientState) {
        this.airwayAndBreathing = new AirwayAndBreathing(patientState.getAirwayAndBreathing());
        this.neurologicalExamination = new NeurologicalExamination(patientState.getNeurologicalExamination());
        this.peripheralWristsEvaluation = new PeripheralWristsEvaluation(patientState.getPeripheralWristsEvaluation());
        this.vascularAccesses = new VascularAccesses(patientState.getVascularAccesses());
        this.vitalSigns = new VitalSignsRNN(patientState.getVitalSigns());
    }

    public static int getNumberOfFields() {
        return AirwayAndBreathing.getNumberOfFields()+NeurologicalExamination.getNumberOfFields()
                +PeripheralWristsEvaluation.getNumberOfFields()+VascularAccesses.getNumberOfFields()
                +VitalSignsRNN.getNumberOfFields();
    }

    public void updateMon(Event event){
        this.vitalSigns.updateMon(event);
    }
    
    public void updateValue(Event event){
        String name=event.getContent().getName();
        String value=event.getContent().getValue();

        try {
            switch (name) {
                // airway And Breathing
                case "Vie Aeree":
                    this.airwayAndBreathing.setAirway(value);
                    break;
                case "Tracheo":
                    this.airwayAndBreathing.setTracheus(value);
                    break;
                case "Inalazione":
                    this.airwayAndBreathing.setInhalation(value);
                    break;
                case "IOT Fallita":
                    this.airwayAndBreathing.setFailedIOT(value);
                    break;
                case "Decompressione Pleurica":
                    this.airwayAndBreathing.setPleuralDecompression(value);
                    break;
                case "% O2":
                    this.airwayAndBreathing.setPercentageO2(value);
                    break;

                // Neurological Examination
                case "GCS Totale":
                    this.neurologicalExamination.setTotalGCS(value);
                    break;
                case "GCS Motorio":
                    this.neurologicalExamination.setMotorGCS(value);
                    break;
                case "GCS Verbale":
                    this.neurologicalExamination.setVerbalGCS(value);
                    break;
                case "GCS Occhi":
                    this.neurologicalExamination.setEyeGCS(value);
                    break;
                case "Sedato": case "Paziente Sedato":
                    this.neurologicalExamination.setSedated(value);
                    break;
                case "Pupille":
                    this.neurologicalExamination.setPupils(value);
                    break;
                case "Deviazione Occhi":
                    this.neurologicalExamination.setEyesDeviations(value);
                    break;
                case "Otorragia":
                    this.neurologicalExamination.setOtorrhagia(value);
                    break;
                case "Motilità Arti Superiori - Destra":
                    this.neurologicalExamination.setMotilityLimb(LimbsEnum.UPPER_RIGHT,value);
                    break;
                case "Motilità Arti Superiori - Sinistra":
                    this.neurologicalExamination.setMotilityLimb(LimbsEnum.UPPER_LEFT,value);
                    break;
                case "Motilità Arti Inferiori - Destra":
                    this.neurologicalExamination.setMotilityLimb(LimbsEnum.LOWER_RIGHT,value);
                    break;
                case "Motilità Arti Inferiori - Sinistra":
                    this.neurologicalExamination.setMotilityLimb(LimbsEnum.LOWER_LEFT,value);
                    break;
                case "Sensibilità Arti Superiori - Destra":
                    this.neurologicalExamination.setSensitivityLimb(LimbsEnum.UPPER_RIGHT,value);
                    break;
                case "Sensibilità Arti Superiori - Sinistra":
                    this.neurologicalExamination.setSensitivityLimb(LimbsEnum.UPPER_LEFT,value);
                    break;
                case "Sensibilità Arti Inferiori - Destra":
                    this.neurologicalExamination.setSensitivityLimb(LimbsEnum.LOWER_RIGHT,value);
                    break;
                case "Sensibilità Arti Inferiori - Sinistra":
                    this.neurologicalExamination.setSensitivityLimb(LimbsEnum.LOWER_LEFT,value);
                    break;
                case "Tono Sfintere":
                    this.neurologicalExamination.setSphincterTone(value);
                    break;
                case "Priapismo":
                    this.neurologicalExamination.setPriapism(value);
                    break;

                // Peripheral Wrists Evaluation
                case "Valutazione Polsi Periferici (Arti Superiori Destra)":
                    this.peripheralWristsEvaluation.setWrist(LimbsEnum.UPPER_RIGHT,value);
                    break;
                case "Valutazione Polsi Periferici (Arti Superiori Sinistra)":
                    this.peripheralWristsEvaluation.setWrist(LimbsEnum.UPPER_LEFT,value);
                    break;
                case "Valutazione Polsi Periferici (Arti Inferiori Destra)":
                    this.peripheralWristsEvaluation.setWrist(LimbsEnum.LOWER_RIGHT,value);
                    break;
                case "Valutazione Polsi Periferici (Arti Inferiori Sinistra)":
                    this.peripheralWristsEvaluation.setWrist(LimbsEnum.LOWER_LEFT,value);
                    break;

                // Vascular Accesses
                case "Periferiche":
                    this.vascularAccesses.setNumberOfPeripheral(value);
                    break;
                case "Intraossea":
                    this.vascularAccesses.setIntraosseus(value);
                    break;
                case "CVC":
                    this.vascularAccesses.setCvc(value);
                    break;

                // vital signs
                case "Temperatura": case "Valore Temperatura":
                    this.vitalSigns.setTemp(value);
                    break;
                case "Frequenza Cardiaca": case "Valore Frequenza Cardiaca":
                    this.vitalSigns.setHR(value);
                    break;
                case "Pressione Arteriosa": case "Valore Pressione Arteriosa":
                    this.vitalSigns.setDIA(value);
                    break;
                case "SYS":
                    this.vitalSigns.setSYS(value);
                    break;
                case "Saturazione (SpO2)": case "Valore Saturazione": case "Saturazione":
                    this.vitalSigns.setSpO2(value);
                    break;
                case "EtCO2": case "Valore EtCO2":
                    this.vitalSigns.setEtCO2(value);
                    break;
                case "Emorragie Esterne":
                    this.vitalSigns.setExternalHemorages(value);
                    break;
                default:
                    System.out.println(name+ " "+value);
            }
        } catch (Exception e) {
            //System.out.println(event.getContent().getValue());
        }
    }

    public AirwayAndBreathing getAirwayAndBreathing() {
        return airwayAndBreathing;
    }

    public NeurologicalExamination getNeurologicalExamination() {
        return neurologicalExamination;
    }

    public PeripheralWristsEvaluation getPeripheralWristsEvaluation() {
        return peripheralWristsEvaluation;
    }

    public VascularAccesses getVascularAccesses() {
        return vascularAccesses;
    }

    public VitalSignsRNN getVitalSigns() {
        return vitalSigns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PatientStateRNN)) return false;
        PatientStateRNN that = (PatientStateRNN) o;
        return Objects.equals(getAirwayAndBreathing(), that.getAirwayAndBreathing()) &&
                Objects.equals(getNeurologicalExamination(), that.getNeurologicalExamination()) &&
                Objects.equals(getPeripheralWristsEvaluation(), that.getPeripheralWristsEvaluation()) &&
                Objects.equals(getVascularAccesses(), that.getVascularAccesses()) &&
                Objects.equals(getVitalSigns(), that.getVitalSigns());
    }


    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        int pos=0;
        double airwayAndBreathingFields[]=airwayAndBreathing.getFields();
        double neurologicalExaminationFields[]=neurologicalExamination.getFields();
        double peripheralWristsEvaluationFields[]=peripheralWristsEvaluation.getFields();
        double vascularAccessesFields[]=vascularAccesses.getFields();
        double vitalSignsFields[]=vitalSigns.getFields();
        System.arraycopy(airwayAndBreathingFields, 0, fields, 0, 
                                        airwayAndBreathingFields.length);
        pos+=airwayAndBreathingFields.length;
        System.arraycopy(neurologicalExaminationFields, 0, fields, pos, 
                                        neurologicalExaminationFields.length);
        pos+=neurologicalExaminationFields.length;
        System.arraycopy(peripheralWristsEvaluationFields, 0, fields, pos,
                                        peripheralWristsEvaluationFields.length);
        pos+=peripheralWristsEvaluationFields.length;
        System.arraycopy(vascularAccessesFields, 0, fields, pos,
                                        vascularAccessesFields.length);
        pos+=vascularAccessesFields.length;
        System.arraycopy(vitalSignsFields, 0, fields, pos,
                                        vitalSignsFields.length);
        return fields;
    }
}
