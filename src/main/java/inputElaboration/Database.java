package inputElaboration;

import com.mongodb.client.*;

import org.bson.Document;
import report.Patient;

import java.util.concurrent.*;

public class Database {

    private static final String DATABASE_NAME="Reports";
    private static final String FULL_COLLECTION_NAME="TraumaTracker";
    private static final String SIMILAR_PATIENTS_TEST_COLLECTION_NAME="SimilarPatientsTest";

    private BlockingDeque<Patient> database;
    private MongoCollection<Document> collection;
    private static Database instance;

    private Database() {
        MongoClient mongoClient=MongoClients.create();
        MongoDatabase mongoDB = mongoClient.getDatabase(DATABASE_NAME);
        this.collection = mongoDB.getCollection(FULL_COLLECTION_NAME);
        this.database=computeDatabase();
        System.out.println("Numero totale di pazienti: " + database.size());
        System.out.println("Numero di pazienti shockati: " + Patient.getNumShocked());
    }

    public static void create() {
        if (instance==null)
            Database.instance=new Database();
    }

    public static Database getInstance() {
        create();
        return instance;
    }

    private BlockingDeque<Patient> computeDatabase(){
        FindIterable<Document> results=this.collection.find();
        BlockingDeque<Patient> database=new LinkedBlockingDeque<>();
        final ExecutorService exec = Executors.newCachedThreadPool();
        for (Document document: results) {
            exec.execute(()-> database.add(new Patient(document)));
        }
        exec.shutdown();
        try {
            exec.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.out.println("ERROR");
            e.printStackTrace();
        }
        return database;
    }

    public BlockingDeque<Patient> getDatabase() {
        return getInstance().database;
    }


}
