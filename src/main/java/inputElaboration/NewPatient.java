package inputElaboration;

import report.Patient;
import utilities.exceptions.InvalidNewPatientSourceException;
import utilities.exceptions.InvalidReportFileException;

import java.io.IOException;

public class NewPatient {
    private String path;
    private Patient newPatient;
    private static NewPatient instance;

    private NewPatient(String path) throws IOException, InvalidReportFileException {
        this.path=path;
        this.newPatient=new Patient(path);
    }

    public static NewPatient getInstance() {
        return instance;
    }

    public String getPath() {
        return instance.path;
    }

    public Patient getNewPatient(){
        return instance.newPatient;
    }

    public static void setPath(String path) throws InvalidNewPatientSourceException, IOException, InvalidReportFileException {
        if (path==null || path.equals(""))
            throw new InvalidNewPatientSourceException();
        if (instance==null || !getInstance().getPath().equals(path))
            NewPatient.instance=new NewPatient(path);
    }
}
