package similarPatientsCalculation;

import utilities.StatisticUtilities;

public class EuclideanMetric implements Metric {
    public static final String name="Euclidean Metric";
    private double totalSquareDistance=0;
    private int nFieldsCompared=0;

    @Override
    public void computeNumbersDistance(Number aN, Number bN, double normalizationFactor){
        if (normalizationFactor==0) //this implies a==b
            return;
        if (aN!=null && bN!=null){
            nFieldsCompared++;
            totalSquareDistance+=StatisticUtilities.getSquareDistance(aN, bN, normalizationFactor);
        }
    }

    @Override
    public void computeObjectsDistance(Object a, Object b){
        if (a!=null || b!=null){
            nFieldsCompared++;
            if ((a!=null && !a.equals(b))||!b.equals(a))
                totalSquareDistance+=StatisticUtilities.DIFFERENT_CATEGORICAL_DISTANCE;
        }
    }

    @Override
    public double computeFinalDistance() {
        return nFieldsCompared>0?Math.sqrt(totalSquareDistance)/nFieldsCompared:1;
    }
}
