package similarPatientsCalculation;

import initialState.LimbsEnum;
import report.Patient;
import initialState.InitialState;
import initialState.*;

public class CalculateDistance {
    private Metric metric;
    private NormalizationEnum normalizationFactor;

    public CalculateDistance(NormalizationEnum normalizationFactor, Metric metric) {
        this.normalizationFactor = normalizationFactor;
        this.metric=metric;
    }

    public double computeDistance(Patient newPatient, Patient databasePatient) {
        InitialState newPatientInitialState=newPatient.getInitialState();
        InitialState databasePatientInitialState=databasePatient.getInitialState();
        vitalSignsSquareDistance(newPatientInitialState.getVitalSigns(),databasePatientInitialState.getVitalSigns());
        airwayAndBreathingSquareDistance(newPatientInitialState.getAirwayAndBreathing(),databasePatientInitialState.getAirwayAndBreathing());
        neurologicalExaminationSquareDistance(newPatientInitialState.getNeurologicalExamination(), databasePatientInitialState.getNeurologicalExamination());
        peripheralWristsSquareDistance(newPatientInitialState.getPeripheralWristsEvaluation(),databasePatientInitialState.getPeripheralWristsEvaluation());
        vascularAccessesSquareDistance(newPatientInitialState.getVascularAccesses(),databasePatientInitialState.getVascularAccesses());
        orthopaedicInjuriesSquareDistance(newPatientInitialState.getOrthopaedicInjuries(), databasePatientInitialState.getOrthopaedicInjuries());
        burnsSquareDistance(newPatientInitialState.getBurns(),databasePatientInitialState.getBurns());
        return metric.computeFinalDistance();
    }

    private void vitalSignsSquareDistance(VitalSigns newPatient, VitalSigns databasePatient){
        if (newPatient.getTemperatureContinue()!=null && databasePatient.getTemperatureContinue()!=null){
            metric.computeNumbersDistance(newPatient.getTemperatureContinue(), databasePatient.getTemperatureContinue(),
                    VitalSigns.getTemperatureNormalization(normalizationFactor));
        } else if (newPatient.getTemperatureDiscrete()!=null && databasePatient.getTemperatureDiscrete()!=null){
            metric.computeObjectsDistance(newPatient.getTemperatureDiscrete(), databasePatient.getTemperatureDiscrete());
        }
        if (newPatient.getHeartRateContinue()!=null && databasePatient.getHeartRateContinue()!=null){
            metric.computeNumbersDistance(newPatient.getHeartRateContinue(), databasePatient.getHeartRateContinue(),
                    VitalSigns.getHeartRateNormalization(normalizationFactor));
        } else if (newPatient.getHeartRateDiscrete()!=null && databasePatient.getHeartRateDiscrete()!=null){
            metric.computeObjectsDistance(newPatient.getHeartRateDiscrete(), databasePatient.getHeartRateDiscrete());
        }
        if (newPatient.getSystolicBloodPressureContinue()!=null && databasePatient.getSystolicBloodPressureContinue()!=null){
            metric.computeNumbersDistance(newPatient.getSystolicBloodPressureContinue(), databasePatient.getSystolicBloodPressureContinue(),
                    VitalSigns.getSystolicBloodPressureNormalization(normalizationFactor));
        } else if (newPatient.getSystolicBloodPressureDiscrete()!=null && databasePatient.getSystolicBloodPressureDiscrete()!=null){
            metric.computeObjectsDistance(newPatient.getSystolicBloodPressureDiscrete(), databasePatient.getSystolicBloodPressureDiscrete());
        }
        if (newPatient.getOxygenSaturationContinue()!=null && databasePatient.getOxygenSaturationContinue()!=null){
            metric.computeNumbersDistance(newPatient.getOxygenSaturationContinue(), databasePatient.getOxygenSaturationContinue(), VitalSigns.getOxygenSaturationNormalization(normalizationFactor));
        } else if (newPatient.getOxygenSaturationDiscrete()!=null && databasePatient.getOxygenSaturationDiscrete()!=null){
            metric.computeObjectsDistance(newPatient.getOxygenSaturationDiscrete(), databasePatient.getOxygenSaturationDiscrete());
        }
        if (newPatient.getEndTidalCO2Continue()!=null && databasePatient.getEndTidalCO2Continue()!=null){
            metric.computeNumbersDistance(newPatient.getEndTidalCO2Continue(), databasePatient.getEndTidalCO2Continue(), VitalSigns.getEndTidalCO2Normalization(normalizationFactor));
        } else if (newPatient.getEndTidalCO2Discrete()!=null && databasePatient.getEndTidalCO2Discrete()!=null){
            metric.computeObjectsDistance(newPatient.getEndTidalCO2Discrete(), databasePatient.getEndTidalCO2Discrete());
        }
        metric.computeObjectsDistance(newPatient.getExternalHemorages(), databasePatient.getExternalHemorages());
        
    }

    private void airwayAndBreathingSquareDistance(AirwayAndBreathing newPatient, AirwayAndBreathing databasePatient) {
        metric.computeObjectsDistance(newPatient.getAirway(), databasePatient.getAirway());
        metric.computeNumbersDistance(newPatient.getPercentageO2(), databasePatient.getPercentageO2(),
                AirwayAndBreathing.getPercentageO2Normalization(normalizationFactor));
        metric.computeObjectsDistance(newPatient.getTracheus(), databasePatient.getTracheus());
        metric.computeObjectsDistance(newPatient.getFailedIOT(), databasePatient.getFailedIOT());
        metric.computeObjectsDistance(newPatient.getInhalation(), databasePatient.getInhalation());
        metric.computeObjectsDistance(newPatient.getPleuralDecompression(), databasePatient.getPleuralDecompression());
    }

    private void neurologicalExaminationSquareDistance(NeurologicalExamination newPatient, NeurologicalExamination databasePatient) {
        metric.computeNumbersDistance(newPatient.getTotalGCS(), databasePatient.getTotalGCS(),
                NeurologicalExamination.getTotalGCSNormalization(normalizationFactor));
        metric.computeNumbersDistance(newPatient.getMotorGCS(), databasePatient.getMotorGCS(),
                NeurologicalExamination.getMotorGCSNormalization(normalizationFactor));
        metric.computeNumbersDistance(newPatient.getVerbalGCS(), databasePatient.getVerbalGCS(),
                NeurologicalExamination.getVerbalGCSNormalization(normalizationFactor));
        metric.computeNumbersDistance(newPatient.getEyeGCS(), databasePatient.getEyeGCS(),
                NeurologicalExamination.getEyeGCSNormalization(normalizationFactor));
        metric.computeObjectsDistance(newPatient.getSedated(),databasePatient.getSedated());
        metric.computeObjectsDistance(newPatient.getPupils(),databasePatient.getPupils());
        metric.computeObjectsDistance(newPatient.getEyesDeviations(), databasePatient.getEyesDeviations());
        metric.computeObjectsDistance(newPatient.getOtorrhagia(), databasePatient.getOtorrhagia());
        for (LimbsEnum limb: LimbsEnum.values()){
            metric.computeObjectsDistance(newPatient.getMotilitySingleLimb(limb), databasePatient.getMotilitySingleLimb(limb));
            metric.computeObjectsDistance(newPatient.getSensitivitySingleLimb(limb), databasePatient.getSensitivitySingleLimb(limb));
        }
        metric.computeObjectsDistance(newPatient.getSphincterTone(), databasePatient.getSphincterTone());
        metric.computeObjectsDistance(newPatient.getPriapism(), databasePatient.getPriapism());
    }

    private void peripheralWristsSquareDistance(PeripheralWristsEvaluation newPatient, PeripheralWristsEvaluation databasePatient){
        for (LimbsEnum limb: LimbsEnum.values())
            metric.computeObjectsDistance(newPatient.getSingleWrist(limb), databasePatient.getSingleWrist(limb));
    }

    private void vascularAccessesSquareDistance(VascularAccesses newPatient, VascularAccesses databasePatient) {
        metric.computeNumbersDistance(newPatient.getNumberOfPeripheral(),databasePatient.getNumberOfPeripheral(),
                VascularAccesses.getNumberOfPeripheralNormalization(normalizationFactor));
        metric.computeObjectsDistance(newPatient.getIntraosseus(),databasePatient.getIntraosseus());
        metric.computeObjectsDistance(newPatient.getCvc(),databasePatient.getCvc());
    }

    private void orthopaedicInjuriesSquareDistance(OrthopaedicInjuries newPatient, OrthopaedicInjuries databasePatient){
        metric.computeObjectsDistance(newPatient.getLimbsFracture(),databasePatient.getLimbsFracture());
        metric.computeObjectsDistance(newPatient.getFractureExposition(),databasePatient.getFractureExposition());
        metric.computeNumbersDistance(newPatient.getFractureGustiloDegree(),databasePatient.getFractureGustiloDegree(),
                OrthopaedicInjuries.getFractureGustiloDegreeNormalization(normalizationFactor));
        metric.computeObjectsDistance(newPatient.getUnstableBasin(),databasePatient.getUnstableBasin());
    }
    
    private void burnsSquareDistance(Burns newPatient, Burns databasePatient){
        metric.computeObjectsDistance(newPatient.getBurns(),databasePatient.getBurns());
        metric.computeNumbersDistance(newPatient.getBurnDegree(),databasePatient.getBurnDegree(),
                Burns.getBurnDegreeNormalization(normalizationFactor));
        metric.computeNumbersDistance(newPatient.getBurnPercentage(),databasePatient.getBurnPercentage(),
                Burns.getBurnPercentageNormalization(normalizationFactor));
    }
    
    
}
