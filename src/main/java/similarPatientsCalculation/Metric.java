package similarPatientsCalculation;

public interface Metric {
    double computeFinalDistance();
    void computeNumbersDistance(Number a, Number b, double normalizationFactor);
    void computeObjectsDistance(Object a, Object b);
}
