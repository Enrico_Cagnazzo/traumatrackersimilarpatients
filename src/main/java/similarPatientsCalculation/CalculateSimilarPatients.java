package similarPatientsCalculation;

import inputElaboration.Database;
import inputElaboration.NewPatient;
import report.Patient;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;

public class CalculateSimilarPatients {
    private static NormalizationEnum normalizationFactor;
    private static Metric metric;

    public static void setParameters(NormalizationEnum normalizationFactor, Metric metric) {
        CalculateSimilarPatients.normalizationFactor=normalizationFactor;
        CalculateSimilarPatients.metric=metric;
    }

    public static Iterable<PatientAndDistance> calculateSimilar() {
        final ExecutorService exec = Executors.newCachedThreadPool();
        BlockingDeque<PatientAndDistance> resultsConcurrent = new LinkedBlockingDeque<>();
        BlockingDeque<Patient> tempList=new LinkedBlockingDeque<>(Database.getInstance().getDatabase());
        while(!tempList.isEmpty()){
            exec.execute(()-> {
                try {
                    Patient patient=tempList.poll();
                    if (patient!=null) {
                        CalculateDistance calculateDistance = new CalculateDistance(normalizationFactor, metric.getClass().getConstructor().newInstance());
                        double distance = calculateDistance.computeDistance(NewPatient.getInstance().getNewPatient(), patient);
                        PatientAndDistance patientAndDistance = new PatientAndDistance(patient, distance);
                        resultsConcurrent.add(patientAndDistance);
                    }
                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            });
        }
        exec.shutdown();
        try {
            exec.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        List<PatientAndDistance> resultsSorted=new ArrayList<>(resultsConcurrent);
        resultsSorted.sort(Comparator.comparingDouble(PatientAndDistance::getDistance));
        return resultsSorted;
    }
}
