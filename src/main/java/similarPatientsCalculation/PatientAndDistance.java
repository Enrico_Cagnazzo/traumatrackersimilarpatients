package similarPatientsCalculation;

import report.Patient;

public class PatientAndDistance {
    private Patient patient;
    private double distance;

    public PatientAndDistance(Patient patient, double distance) {
        this.patient = patient;
        this.distance = distance;
    }

    public Patient getPatient() {
        return patient;
    }

    public double getDistance() {
        return distance;
    }

}
