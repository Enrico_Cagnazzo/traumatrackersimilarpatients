package similarPatientsCalculation;

import utilities.StatisticUtilities;

public class UnifiedMetricCheungJia implements Metric {
    public static final String name="Unified metric by Cheung and Jia";
    private static final int SIMILARITY_COEFFICIENT=1;
    //As defined in: "Generalized Similarity Measure for Categorical Data Clustering" of Sharma and Singh

    private int nCategoricalFieldsCompared=0;
    private int nIntegerFieldsCompared=0;
    private double numericalSquareDistance=0;
    private int nSameCategoricalFields=0;

    @Override
    public double computeFinalDistance() {
        int nDifferentCategoricalFields=nCategoricalFieldsCompared-nSameCategoricalFields;

        double categoricalDistance=0;
        if (nCategoricalFieldsCompared>0)
            categoricalDistance=1-((double)nSameCategoricalFields)/
                    (nSameCategoricalFields+SIMILARITY_COEFFICIENT*nDifferentCategoricalFields);
        double numericalDistance=0;
        if (nIntegerFieldsCompared>0) {
            numericalDistance=Math.sqrt(numericalSquareDistance)/nIntegerFieldsCompared;
            numericalDistance=1-Math.exp(-numericalDistance);
        }
        return nCategoricalFieldsCompared+nIntegerFieldsCompared>0
                ?categoricalDistance+numericalDistance
                :1;
    }

    @Override
    public void computeNumbersDistance(Number aN, Number bN, double normalizationFactor){
        if (normalizationFactor==0) //this implies a==b
            return;
        if (aN!=null && bN!=null){
            nIntegerFieldsCompared++;
            numericalSquareDistance+=StatisticUtilities.getSquareDistance(aN, bN, normalizationFactor);
        }
    }

    @Override
    public void computeObjectsDistance(Object a, Object b) {
        if (a!=null || b!=null){
            nCategoricalFieldsCompared++;
            if (a!=null && a.equals(b))
                nSameCategoricalFields++;
        }
    }
}
