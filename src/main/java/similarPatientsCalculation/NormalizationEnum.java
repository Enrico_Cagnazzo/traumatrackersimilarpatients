package similarPatientsCalculation;

public enum NormalizationEnum {
    MAX_MIN,
    MEAN_VARIANCE;

    @Override
    public String toString() {
        return this.name();
    }

}
