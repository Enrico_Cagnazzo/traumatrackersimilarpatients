package initialState;

import similarPatientsCalculation.NormalizationEnum;
import utilities.StatisticUtilities;
import utilities.StringUtilities;
import initialState.enumInitialState.BurnsEnum;
import report.StartVitalSigns;

import java.util.HashSet;
import java.util.Set;

public class Burns {

    private static Integer burnDegreeMaxValue;
    private static Integer burnDegreeMinValue;
    private static Set<Number> burnDegreeValues=new HashSet<>();
    private static double[] burnDegreeNormalization = new double[NormalizationEnum.values().length];

    private static Integer burnPercentageMinValue;
    private static Integer burnPercentageMaxValue;
    private static Set<Number> burnPercentageValues=new HashSet<>();
    private static double[] burnPercentageNormalization = new double[NormalizationEnum.values().length];

    private BurnsEnum burns;
    private Integer burnDegree;
    private Integer burnPercentage;

    public Burns(StartVitalSigns startVitalSigns) {
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getBurn())) {
            this.burns=BurnsEnum.getValue(startVitalSigns.getBurn());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getBurnDegree())){
            this.burnDegree=Integer.parseInt(startVitalSigns.getBurnDegree().substring(0,1));
            Burns.burnDegreeValues.add(this.burnDegree);
            if (Burns.burnDegreeMinValue==null || this.burnDegree<Burns.burnDegreeMinValue)
                Burns.burnDegreeMinValue=this.burnDegree;
            if (Burns.burnDegreeMaxValue==null || this.burnDegree>Burns.burnDegreeMaxValue)
                Burns.burnDegreeMaxValue=this.burnDegree;
            
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getBurnPercentage())){
            this.burnPercentage=Integer.parseInt(startVitalSigns.getBurnPercentage());
            Burns.burnPercentageValues.add(this.burnPercentage);
            if (Burns.burnPercentageMinValue==null || this.burnPercentage<Burns.burnPercentageMinValue)
                Burns.burnPercentageMinValue=this.burnPercentage;
            if (Burns.burnPercentageMaxValue==null || this.burnPercentage>Burns.burnPercentageMaxValue)
                Burns.burnPercentageMaxValue=this.burnPercentage;

        }
    }

    public BurnsEnum getBurns() {
        return burns;
    }

    public Integer getBurnDegree() {
        return burnDegree;
    }

    public Integer getBurnPercentage() {
        return burnPercentage;
    }

    public static void computeNormalizationFactors() {
        Burns.burnDegreeNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()]=
                StatisticUtilities.computeVariance(Burns.burnDegreeValues);
        Burns.burnPercentageNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()]=
                StatisticUtilities.computeVariance(Burns.burnPercentageValues);
        Burns.burnDegreeNormalization[NormalizationEnum.MAX_MIN.ordinal()]=StatisticUtilities.computeDistance(
                Burns.burnDegreeMaxValue,Burns.burnDegreeMinValue);
        Burns.burnPercentageNormalization[NormalizationEnum.MAX_MIN.ordinal()]=StatisticUtilities.computeDistance(
                Burns.burnPercentageMaxValue,Burns.burnPercentageMinValue);
    }

    public static double getBurnDegreeNormalization(NormalizationEnum normalizationFactor) {
        return Burns.burnDegreeNormalization[normalizationFactor.ordinal()];
    }

    public static double getBurnPercentageNormalization(NormalizationEnum normalizationFactor) {
        return Burns.burnPercentageNormalization[normalizationFactor.ordinal()];
    }


}
