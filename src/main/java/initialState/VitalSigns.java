package initialState;

import similarPatientsCalculation.NormalizationEnum;
import utilities.StatisticUtilities;
import utilities.StringUtilities;
import initialState.enumInitialState.vitalSigns.*;
import report.StartVitalSigns;

import java.util.HashSet;
import java.util.Set;

public class VitalSigns {
    private static Double temperatureMinValue;
    private static Double temperatureMaxValue;
    private static Set<Number> temperatureValues = new HashSet<>();
    private static double[] temperatureNormalization = new double[NormalizationEnum.values().length];

    private static Integer heartRateMinValue;
    private static Integer heartRateMaxValue;
    private static Set<Number> heartRateValues = new HashSet<>();
    private static double[] heartRateNormalization = new double[NormalizationEnum.values().length];

    private static Integer systolicBloodPressureMinValue;
    private static Integer systolicBloodPressureMaxValue;
    private static Set<Number> systolicBloodPressureValues = new HashSet<>();
    private static double[] systolicBloodPressureNormalization = new double[NormalizationEnum.values().length];

    private static Integer oxygenSaturationMinValue;
    private static Integer oxygenSaturationMaxValue;
    private static Set<Number> oxygenSaturationValues = new HashSet<>();
    private static double[] oxygenSaturationNormalization = new double[NormalizationEnum.values().length];

    private static Double endTidalCO2MinValue;
    private static Double endTidalCO2MaxValue;
    private static Set<Number> endTidalCO2Values = new HashSet<>();
    private static double[] endTidalCO2Normalization = new double[NormalizationEnum.values().length];
    
    private Double temperatureContinue;
    private TemperatureEnum temperatureDiscrete;
    private Integer heartRateContinue;
    private HeartRateEnum heartRateDiscrete;
    private Integer systolicBloodPressureContinue;
    private SystolicBloodPressureEnum systolicBloodPressureDiscrete;
    private Integer oxygenSaturationContinue;
    private OxygenSaturationEnum oxygenSaturationDiscrete;
    private Double endTidalCO2Continue;
    private EndTidalCO2Enum endTidalCO2Discrete;
    private Boolean externalHemorages;

    public VitalSigns(StartVitalSigns startVitalSigns) {

        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getTempValue())){
            this.temperatureContinue=Double.parseDouble(startVitalSigns.getTempValue());
            VitalSigns.temperatureValues.add(this.temperatureContinue);
            if (VitalSigns.temperatureMinValue==null || this.temperatureContinue<VitalSigns.temperatureMinValue)
                VitalSigns.temperatureMinValue=this.temperatureContinue;
            if (VitalSigns.temperatureMaxValue==null || this.temperatureContinue>VitalSigns.temperatureMaxValue)
                VitalSigns.temperatureMaxValue=this.temperatureContinue;
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getTemp()))
            this.temperatureDiscrete = TemperatureEnum.getValue(startVitalSigns.getTemp());
        
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getHRValue())){
            this.heartRateContinue=Integer.parseInt(startVitalSigns.getHRValue());
            VitalSigns.heartRateValues.add(this.heartRateContinue);
            if (VitalSigns.heartRateMinValue==null || this.heartRateContinue<VitalSigns.heartRateMinValue)
                VitalSigns.heartRateMinValue=this.heartRateContinue;
            if (VitalSigns.heartRateMaxValue==null || this.heartRateContinue>VitalSigns.heartRateMaxValue)
                VitalSigns.heartRateMaxValue=this.heartRateContinue;
        } 
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getHR()))
            this.heartRateDiscrete =HeartRateEnum.getValue(startVitalSigns.getHR());
        
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getBPValue())) {
            this.systolicBloodPressureContinue=Integer.parseInt(startVitalSigns.getBPValue());
            VitalSigns.systolicBloodPressureValues.add(this.systolicBloodPressureContinue);
            if (VitalSigns.systolicBloodPressureMinValue==null || this.systolicBloodPressureContinue<VitalSigns.systolicBloodPressureMinValue)
                VitalSigns.systolicBloodPressureMinValue=this.systolicBloodPressureContinue;
            if (VitalSigns.systolicBloodPressureMaxValue==null || this.systolicBloodPressureContinue>VitalSigns.systolicBloodPressureMaxValue)
                VitalSigns.systolicBloodPressureMaxValue=this.systolicBloodPressureContinue;
        } 
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getBP()))
            this.systolicBloodPressureDiscrete=SystolicBloodPressureEnum.getValue(startVitalSigns.getBP());
        
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getSpO2Value())){
            this.oxygenSaturationContinue=Integer.parseInt(startVitalSigns.getSpO2Value());
            VitalSigns.oxygenSaturationValues.add(this.oxygenSaturationContinue);
            if (VitalSigns.oxygenSaturationMinValue==null || this.oxygenSaturationContinue<VitalSigns.oxygenSaturationMinValue)
                VitalSigns.oxygenSaturationMinValue=this.oxygenSaturationContinue;
            if (VitalSigns.oxygenSaturationMaxValue==null || this.oxygenSaturationContinue>VitalSigns.oxygenSaturationMaxValue)
                VitalSigns.oxygenSaturationMaxValue=this.oxygenSaturationContinue;
        } 
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getSpO2()))
            this.oxygenSaturationDiscrete=OxygenSaturationEnum.getValue(startVitalSigns.getSpO2());
        
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getEtCO2Value())) {
            this.endTidalCO2Continue = Double.parseDouble(startVitalSigns.getEtCO2Value());
            VitalSigns.endTidalCO2Values.add(this.endTidalCO2Continue);
            if (VitalSigns.endTidalCO2MinValue==null || this.endTidalCO2Continue<VitalSigns.endTidalCO2MinValue)
                VitalSigns.endTidalCO2MinValue=this.endTidalCO2Continue;
            if (VitalSigns.endTidalCO2MaxValue==null || this.endTidalCO2Continue>VitalSigns.endTidalCO2MaxValue)
                VitalSigns.endTidalCO2MaxValue=this.endTidalCO2Continue;
        } 
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getEtCO2()))
            this.endTidalCO2Discrete =EndTidalCO2Enum.getValue(startVitalSigns.getEtCO2());
        
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getExtBleeding())){
            this.externalHemorages=startVitalSigns.getExtBleeding().equals("Si");
        }
    }

    public static void computeNormalizationFactors() {
        VitalSigns.endTidalCO2Normalization[NormalizationEnum.MEAN_VARIANCE.ordinal()]=
                StatisticUtilities.computeVariance(VitalSigns.endTidalCO2Values);
        VitalSigns.endTidalCO2Normalization[NormalizationEnum.MAX_MIN.ordinal()]=StatisticUtilities.computeDistance(
                VitalSigns.endTidalCO2MaxValue,VitalSigns.endTidalCO2MinValue);
        VitalSigns.heartRateNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()]=
                StatisticUtilities.computeVariance(VitalSigns.heartRateValues);
        VitalSigns.heartRateNormalization[NormalizationEnum.MAX_MIN.ordinal()]=StatisticUtilities.computeDistance(
                VitalSigns.heartRateMaxValue,VitalSigns.heartRateMinValue);
        VitalSigns.oxygenSaturationNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()]=
                StatisticUtilities.computeVariance(VitalSigns.oxygenSaturationValues);
        VitalSigns.oxygenSaturationNormalization[NormalizationEnum.MAX_MIN.ordinal()]=StatisticUtilities.computeDistance(
                VitalSigns.oxygenSaturationMaxValue,VitalSigns.oxygenSaturationMinValue);
        VitalSigns.systolicBloodPressureNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()]=
                StatisticUtilities.computeVariance(VitalSigns.systolicBloodPressureValues);
        VitalSigns.systolicBloodPressureNormalization[NormalizationEnum.MAX_MIN.ordinal()]=StatisticUtilities.computeDistance(
                VitalSigns.systolicBloodPressureMaxValue,VitalSigns.systolicBloodPressureMinValue);
        VitalSigns.temperatureNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()]=
                StatisticUtilities.computeVariance(VitalSigns.temperatureValues);
        VitalSigns.temperatureNormalization[NormalizationEnum.MAX_MIN.ordinal()]=StatisticUtilities.computeDistance(
                VitalSigns.temperatureMaxValue, VitalSigns.temperatureMinValue);
    }

    public Double getTemperatureContinue() {
        return temperatureContinue;
    }

    public TemperatureEnum getTemperatureDiscrete() {
        return temperatureDiscrete;
    }

    public Integer getHeartRateContinue() {
        return heartRateContinue;
    }

    public HeartRateEnum getHeartRateDiscrete() {
        return heartRateDiscrete;
    }

    public Integer getSystolicBloodPressureContinue() {
        return systolicBloodPressureContinue;
    }

    public SystolicBloodPressureEnum getSystolicBloodPressureDiscrete() {
        return systolicBloodPressureDiscrete;
    }

    public Integer getOxygenSaturationContinue() {
        return oxygenSaturationContinue;
    }

    public OxygenSaturationEnum getOxygenSaturationDiscrete() {
        return oxygenSaturationDiscrete;
    }

    public Double getEndTidalCO2Continue() {
        return endTidalCO2Continue;
    }

    public EndTidalCO2Enum getEndTidalCO2Discrete() {
        return endTidalCO2Discrete;
    }

    public Boolean getExternalHemorages() {
        return externalHemorages;
    }

    public static double getTemperatureNormalization(NormalizationEnum normalizationFactor) {
        return temperatureNormalization[normalizationFactor.ordinal()];
    }
    
    public static double getHeartRateNormalization(NormalizationEnum normalizationFactor) {
        return heartRateNormalization[normalizationFactor.ordinal()];
    }
    
    public static double getSystolicBloodPressureNormalization(NormalizationEnum normalizationFactor) {
        return systolicBloodPressureNormalization[normalizationFactor.ordinal()];
    }
    
    public static double getOxygenSaturationNormalization(NormalizationEnum normalizationFactor) {
        return oxygenSaturationNormalization[normalizationFactor.ordinal()];
    }
    
    public static double getEndTidalCO2Normalization(NormalizationEnum normalizationFactor) {
        return endTidalCO2Normalization[normalizationFactor.ordinal()];
    }

}
