package initialState;

import similarPatientsCalculation.NormalizationEnum;
import utilities.RNNUtilities;
import utilities.StatisticUtilities;
import utilities.StringUtilities;
import initialState.enumInitialState.neurologicalExamination.EyesDeviationsEnum;
import initialState.enumInitialState.neurologicalExamination.OtorrhagiaEnum;
import initialState.enumInitialState.neurologicalExamination.PupilsEnum;
import report.StartVitalSigns;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class NeurologicalExamination {

    private static Integer totalGCSMaxValue;
    private static Integer totalGCSMinValue;
    private static Set<Number> totalGCSValues=new HashSet<>();
    private static double[] totalGCSNormalization = new double[NormalizationEnum.values().length];

    private static Integer motorGCSMinValue;
    private static Integer motorGCSMaxValue;
    private static Set<Number> motorGCSValues=new HashSet<>();
    private static double[] motorGCSNormalization = new double[NormalizationEnum.values().length];

    private static Integer verbalGCSMinValue;
    private static Integer verbalGCSMaxValue;
    private static Set<Number> verbalGCSValues=new HashSet<>();
    private static double[] verbalGCSNormalization = new double[NormalizationEnum.values().length];

    private static Integer eyeGCSMinValue;
    private static Integer eyeGCSMaxValue;
    private static Set<Number> eyeGCSValues=new HashSet<>();
    private static double[] eyeGCSNormalization = new double[NormalizationEnum.values().length];

    private Integer totalGCS;
    private Integer motorGCS;
    private Integer verbalGCS;
    private Integer eyeGCS;
    private Boolean sedated;
    private PupilsEnum pupils;
    private EyesDeviationsEnum eyesDeviations;
    private OtorrhagiaEnum otorrhagia;
    private Boolean[] motilityLimbs=new Boolean[4];
    private Boolean[] sensitivityLimbs=new Boolean[4];
    private Boolean sphincterTone;
    private Boolean priapism;

    public NeurologicalExamination(StartVitalSigns startVitalSigns) {
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getGCSTotal())) {
            this.setTotalGCS(startVitalSigns.getGCSTotal());
            NeurologicalExamination.totalGCSValues.add(this.totalGCS);
            if (NeurologicalExamination.totalGCSMinValue==null || this.totalGCS<NeurologicalExamination.totalGCSMinValue)
                NeurologicalExamination.totalGCSMinValue=this.totalGCS;
            if (NeurologicalExamination.totalGCSMaxValue==null || this.totalGCS>NeurologicalExamination.totalGCSMaxValue)
                NeurologicalExamination.totalGCSMaxValue=this.totalGCS;
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getGCSMotor())){
            this.setMotorGCS(startVitalSigns.getGCSMotor());
            NeurologicalExamination.motorGCSValues.add(this.motorGCS);
            if (NeurologicalExamination.motorGCSMinValue==null || this.motorGCS<NeurologicalExamination.motorGCSMinValue)
                NeurologicalExamination.motorGCSMinValue=this.motorGCS;
            if (NeurologicalExamination.motorGCSMaxValue==null || this.motorGCS>NeurologicalExamination.motorGCSMaxValue)
                NeurologicalExamination.motorGCSMaxValue=this.motorGCS;
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getGCSVerbal())){
            this.setVerbalGCS(startVitalSigns.getGCSVerbal());
            NeurologicalExamination.verbalGCSValues.add(this.verbalGCS);
            if (NeurologicalExamination.verbalGCSMinValue==null || this.verbalGCS<NeurologicalExamination.verbalGCSMinValue)
                NeurologicalExamination.verbalGCSMinValue=this.verbalGCS;
            if (NeurologicalExamination.verbalGCSMaxValue==null || this.verbalGCS>NeurologicalExamination.verbalGCSMaxValue)
                NeurologicalExamination.verbalGCSMaxValue=this.verbalGCS;
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getGCSEyes())){
            this.setEyeGCS(startVitalSigns.getGCSEyes());
            NeurologicalExamination.eyeGCSValues.add(this.eyeGCS);
            if (NeurologicalExamination.eyeGCSMinValue==null || this.eyeGCS<NeurologicalExamination.eyeGCSMinValue)
                NeurologicalExamination.eyeGCSMinValue=this.eyeGCS;
            if (NeurologicalExamination.eyeGCSMaxValue==null || this.eyeGCS>NeurologicalExamination.eyeGCSMaxValue)
                NeurologicalExamination.eyeGCSMaxValue=this.eyeGCS;
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getSedated())){
            this.setSedated(startVitalSigns.getSedated());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getPupils())){
            this.setPupils(startVitalSigns.getPupils());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getEyesDeviation())){
            this.setEyesDeviations(startVitalSigns.getEyesDeviation());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getEarsBlood())){
            this.setOtorrhagia(startVitalSigns.getEarsBlood());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getUpperLimbsLeftMotility())){
            this.setMotilityLimb(LimbsEnum.UPPER_LEFT, startVitalSigns.getUpperLimbsLeftMotility());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getUpperLimbsRightMotility())){
            this.setMotilityLimb(LimbsEnum.UPPER_RIGHT, startVitalSigns.getUpperLimbsRightMotility());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getLowerLimbsLeftMotility())){
            this.setMotilityLimb(LimbsEnum.LOWER_LEFT, startVitalSigns.getLowerLimbsLeftMotility());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getLowerLimbsRightMotility())){
            this.setMotilityLimb(LimbsEnum.LOWER_RIGHT, startVitalSigns.getLowerLimbsRightMotility());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getUpperLimbsLeftSensitivity())){
            this.setSensitivityLimb(LimbsEnum.UPPER_LEFT, startVitalSigns.getUpperLimbsLeftSensitivity());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getUpperLimbsRightSensitivity())){
            this.setSensitivityLimb(LimbsEnum.UPPER_RIGHT, startVitalSigns.getUpperLimbsRightSensitivity());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getLowerLimbsLeftSensitivity())){
            this.setSensitivityLimb(LimbsEnum.LOWER_LEFT, startVitalSigns.getLowerLimbsLeftSensitivity());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getLowerLimbsRightSensitivity())){
            this.setSensitivityLimb(LimbsEnum.LOWER_RIGHT, startVitalSigns.getLowerLimbsRightSensitivity());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getSphincterTone())){
            this.setSphincterTone(startVitalSigns.getSphincterTone());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getPriapism())){
            this.setPriapism(startVitalSigns.getPriapism());
        }

    }

    public NeurologicalExamination(NeurologicalExamination neurologicalExamination) {
        this.totalGCS=neurologicalExamination.getTotalGCS();
        this.motorGCS=neurologicalExamination.getMotorGCS();
        this.verbalGCS=neurologicalExamination.getVerbalGCS();
        this.eyeGCS=neurologicalExamination.getEyeGCS();
        this.sedated=neurologicalExamination.getSedated();
        this.pupils=neurologicalExamination.getPupils();
        this.eyesDeviations=neurologicalExamination.getEyesDeviations();
        this.otorrhagia=neurologicalExamination.getOtorrhagia();
        this.motilityLimbs=neurologicalExamination.getMotilityLimbs();
        this.sensitivityLimbs=neurologicalExamination.getSensitivityLimbs();
        this.sphincterTone=neurologicalExamination.getSphincterTone();
        this.priapism=neurologicalExamination.getPriapism();
    }

    public static int getNumberOfFields() {
        return 18;
    }

    public Integer getTotalGCS() {
        return totalGCS;
    }

    public Integer getMotorGCS() {
        return motorGCS;
    }

    public Integer getVerbalGCS() {
        return verbalGCS;
    }

    public Integer getEyeGCS() {
        return eyeGCS;
    }

    public Boolean getSedated() {
        return sedated;
    }

    public PupilsEnum getPupils() {
        return pupils;
    }

    public EyesDeviationsEnum getEyesDeviations() {
        return eyesDeviations;
    }

    public OtorrhagiaEnum getOtorrhagia() {
        return otorrhagia;
    }

    public Boolean getMotilitySingleLimb(LimbsEnum limb){
        return motilityLimbs[limb.ordinal()];
    }

    public Boolean getSensitivitySingleLimb(LimbsEnum limb){
        return sensitivityLimbs[limb.ordinal()];
    }

    public Boolean getSphincterTone() {
        return sphincterTone;
    }

    public Boolean getPriapism() {
        return priapism;
    }

    public void setTotalGCS(String totalGCS) {
        this.totalGCS = Integer.parseInt(totalGCS);
    }

    public void setMotorGCS(String motorGCS) {
        this.motorGCS=Integer.parseInt(motorGCS.substring(1,2));
    }

    public void setVerbalGCS(String verbalGCS) {
        this.verbalGCS =Integer.parseInt(verbalGCS.substring(1,2));
    }

    public void setEyeGCS(String eyeGCS) {
        this.eyeGCS =Integer.parseInt(eyeGCS.substring(1,2));
    }

    public void setSedated(String sedated) {
        this.sedated = sedated.equals("Si");
    }

    public void setPupils(String pupils) {
        this.pupils = PupilsEnum.getValue(pupils);
    }

    public void setEyesDeviations(String eyesDeviations) {
        this.eyesDeviations =EyesDeviationsEnum.getValue(eyesDeviations);
    }

    public void setOtorrhagia(String otorrhagia) {
        this.otorrhagia=OtorrhagiaEnum.getValue(otorrhagia);
    }

    public void setMotilityLimb(LimbsEnum limb, String motilityLimb) {
        this.motilityLimbs[limb.ordinal()] = motilityLimb.equals("Si");
    }

    public void setSensitivityLimb(LimbsEnum limb, String sensitivityLimb) {
        this.sensitivityLimbs[limb.ordinal()] = sensitivityLimb.equals("Si");
    }

    public void setSphincterTone(String sphincterTone) {
        this.sphincterTone = sphincterTone.equals("Si");
    }

    public void setPriapism(String priapism) {
        this.priapism = priapism.equals("Si");
    }

    public static void computeNormalizationFactors() {
        NeurologicalExamination.totalGCSNormalization[NormalizationEnum.MAX_MIN.ordinal()] =StatisticUtilities.computeDistance(
                NeurologicalExamination.totalGCSMaxValue,NeurologicalExamination.totalGCSMinValue);
        NeurologicalExamination.totalGCSNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()] =
                StatisticUtilities.computeVariance(NeurologicalExamination.totalGCSValues);
        NeurologicalExamination.motorGCSNormalization[NormalizationEnum.MAX_MIN.ordinal()] =StatisticUtilities.computeDistance(
                NeurologicalExamination.motorGCSMaxValue,NeurologicalExamination.motorGCSMinValue);
        NeurologicalExamination.motorGCSNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()] =
                StatisticUtilities.computeVariance(NeurologicalExamination.motorGCSValues);
        NeurologicalExamination.verbalGCSNormalization[NormalizationEnum.MAX_MIN.ordinal()] =StatisticUtilities.computeDistance(
                NeurologicalExamination.verbalGCSMaxValue,NeurologicalExamination.verbalGCSMinValue);
        NeurologicalExamination.verbalGCSNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()] =
                StatisticUtilities.computeVariance(NeurologicalExamination.verbalGCSValues);
        NeurologicalExamination.eyeGCSNormalization[NormalizationEnum.MAX_MIN.ordinal()] =StatisticUtilities.computeDistance(
                NeurologicalExamination.eyeGCSMaxValue,NeurologicalExamination.eyeGCSMinValue);
        NeurologicalExamination.eyeGCSNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()] =
                StatisticUtilities.computeVariance(NeurologicalExamination.eyeGCSValues);
    }

    public static double getTotalGCSNormalization(NormalizationEnum normalizationFactor) {
        return totalGCSNormalization[normalizationFactor.ordinal()];
    }
    public static double getMotorGCSNormalization(NormalizationEnum normalizationFactor) {
        return motorGCSNormalization[normalizationFactor.ordinal()];
    }
    public static double getVerbalGCSNormalization(NormalizationEnum normalizationFactor) {
        return verbalGCSNormalization[normalizationFactor.ordinal()];
    }
    public static double getEyeGCSNormalization(NormalizationEnum normalizationFactor) {
        return eyeGCSNormalization[normalizationFactor.ordinal()];
    }

    public Boolean[] getMotilityLimbs() {
        return motilityLimbs.clone();
    }

    public Boolean[] getSensitivityLimbs() {
        return sensitivityLimbs.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NeurologicalExamination)) return false;
        NeurologicalExamination that = (NeurologicalExamination) o;
        return Objects.equals(getTotalGCS(), that.getTotalGCS()) &&
                Objects.equals(getMotorGCS(), that.getMotorGCS()) &&
                Objects.equals(getVerbalGCS(), that.getVerbalGCS()) &&
                Objects.equals(getEyeGCS(), that.getEyeGCS()) &&
                Objects.equals(getSedated(), that.getSedated()) &&
                getPupils() == that.getPupils() &&
                getEyesDeviations() == that.getEyesDeviations() &&
                getOtorrhagia() == that.getOtorrhagia() &&
                Arrays.equals(motilityLimbs, that.motilityLimbs) &&
                Arrays.equals(sensitivityLimbs, that.sensitivityLimbs) &&
                Objects.equals(getSphincterTone(), that.getSphincterTone()) &&
                Objects.equals(getPriapism(), that.getPriapism());
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        fields[0]=RNNUtilities.getDoubleFromInteger(totalGCS);
        fields[1]=RNNUtilities.getDoubleFromInteger(motorGCS);
        fields[2]=RNNUtilities.getDoubleFromInteger(verbalGCS);
        fields[3]=RNNUtilities.getDoubleFromInteger(eyeGCS);
        fields[4]=RNNUtilities.getDoubleFromBoolean(sedated);
        fields[5]=RNNUtilities.getDoubleFromEnum(pupils);
        fields[6]=RNNUtilities.getDoubleFromEnum(eyesDeviations);
        fields[7]=RNNUtilities.getDoubleFromEnum(otorrhagia);
        fields[8]=RNNUtilities.getDoubleFromBoolean(motilityLimbs[0]);
        fields[9]=RNNUtilities.getDoubleFromBoolean(motilityLimbs[1]);
        fields[10]=RNNUtilities.getDoubleFromBoolean(motilityLimbs[2]);
        fields[11]=RNNUtilities.getDoubleFromBoolean(motilityLimbs[3]);
        fields[12]=RNNUtilities.getDoubleFromBoolean(sensitivityLimbs[0]);
        fields[13]=RNNUtilities.getDoubleFromBoolean(sensitivityLimbs[1]);
        fields[14]=RNNUtilities.getDoubleFromBoolean(sensitivityLimbs[2]);
        fields[15]=RNNUtilities.getDoubleFromBoolean(sensitivityLimbs[3]);
        fields[16]=RNNUtilities.getDoubleFromBoolean(sphincterTone);
        fields[17]=RNNUtilities.getDoubleFromBoolean(priapism);
        return fields;
    }
}
