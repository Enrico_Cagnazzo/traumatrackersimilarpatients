package initialState;

import report.StartVitalSigns;
import similarPatientsCalculation.NormalizationEnum;
import utilities.StatisticUtilities;
import utilities.StringUtilities;

import java.util.HashSet;
import java.util.Set;

public class OrthopaedicInjuries {
    private static Integer fractureGustiloDegreeMinValue;
    private static Integer fractureGustiloDegreeMaxValue;
    private static Set<Number> fractureGustiloDegreeValues=new HashSet<>();
    private static double[] fractureGustiloDegreeNormalization = new double[NormalizationEnum.values().length];

    private Boolean limbsFracture;
    private Boolean fractureExposition;
    private Integer fractureGustiloDegree;
    private Boolean unstableBasin;

    public OrthopaedicInjuries(StartVitalSigns startVitalSigns) {
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getLimbsFracture())){
            this.limbsFracture=startVitalSigns.getLimbsFracture().equals("Si");
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getFractureExposition())){
            this.fractureExposition=startVitalSigns.getFractureExposition().equals("Si");
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getFractureGustiloDegree())){
            this.fractureGustiloDegree=0;
            for (int i = 0; i < startVitalSigns.getFractureGustiloDegree().length() - 1; i++)
                if (startVitalSigns.getFractureGustiloDegree().charAt(i) == 'I')
                    this.fractureGustiloDegree++;
            OrthopaedicInjuries.fractureGustiloDegreeValues.add(this.fractureGustiloDegree);
            if (OrthopaedicInjuries.fractureGustiloDegreeMinValue==null || this.fractureGustiloDegree<OrthopaedicInjuries.fractureGustiloDegreeMinValue)
                OrthopaedicInjuries.fractureGustiloDegreeMinValue=this.fractureGustiloDegree;
            if (OrthopaedicInjuries.fractureGustiloDegreeMaxValue==null || this.fractureGustiloDegree>OrthopaedicInjuries.fractureGustiloDegreeMaxValue)
                OrthopaedicInjuries.fractureGustiloDegreeMaxValue=this.fractureGustiloDegree;
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getUnstableBasin())){
            this.unstableBasin=startVitalSigns.getUnstableBasin().equals("Si");
        }
    }

    public Boolean getLimbsFracture() {
        return limbsFracture;
    }

    public Boolean getFractureExposition() {
        return fractureExposition;
    }

    public Integer getFractureGustiloDegree() {
        return fractureGustiloDegree;
    }

    public Boolean getUnstableBasin() {
        return unstableBasin;
    }

    public static void computeNormalizationFactors() {
        OrthopaedicInjuries.fractureGustiloDegreeNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()] =
                StatisticUtilities.computeVariance(OrthopaedicInjuries.fractureGustiloDegreeValues);
        OrthopaedicInjuries.fractureGustiloDegreeNormalization[NormalizationEnum.MAX_MIN.ordinal()] =
                StatisticUtilities.computeDistance(OrthopaedicInjuries.fractureGustiloDegreeMaxValue,
                                                    OrthopaedicInjuries.fractureGustiloDegreeMinValue);
    }

    public static double getFractureGustiloDegreeNormalization(NormalizationEnum normalizationFactor) {
        return OrthopaedicInjuries.fractureGustiloDegreeNormalization[normalizationFactor.ordinal()];
    }

}
