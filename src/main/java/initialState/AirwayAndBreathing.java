package initialState;

import similarPatientsCalculation.NormalizationEnum;
import utilities.RNNUtilities;
import utilities.StatisticUtilities;
import utilities.StringUtilities;
import initialState.enumInitialState.airwaysAndBreathing.AirwayEnum;
import initialState.enumInitialState.airwaysAndBreathing.PleuralDecompressionEnum;
import report.StartVitalSigns;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class AirwayAndBreathing {
    private static Integer percentageO2MinValue;
    private static Integer percentageO2MaxValue;
    private static Set<Number> percentageO2Values = new HashSet<>();
    private static double[] percentageO2Normalization = new double[NormalizationEnum.values().length];

    private AirwayEnum airway;
    private Integer percentageO2;
    private Boolean tracheus;
    private Boolean failedIOT;
    private Boolean inhalation;
    private PleuralDecompressionEnum pleuralDecompression;

    public AirwayAndBreathing(StartVitalSigns startVitalSigns) {
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getAirway())) {
            this.setAirway(startVitalSigns.getAirway());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getOxygenPercentage())) {
            this.setPercentageO2(startVitalSigns.getOxygenPercentage());
            AirwayAndBreathing.percentageO2Values.add(this.percentageO2);
            if (AirwayAndBreathing.percentageO2MinValue == null || this.percentageO2 < AirwayAndBreathing.percentageO2MinValue)
                AirwayAndBreathing.percentageO2MinValue = this.percentageO2;
            if (AirwayAndBreathing.percentageO2MaxValue == null || this.percentageO2 > AirwayAndBreathing.percentageO2MaxValue)
                AirwayAndBreathing.percentageO2MaxValue = this.percentageO2;
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getTracheo())) {
            this.setTracheus(startVitalSigns.getTracheo());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getIntubationFailed())) {
            this.setFailedIOT(startVitalSigns.getIntubationFailed());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getInhalation())) {
            this.setInhalation(startVitalSigns.getInhalation());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getChestTube())) {
            this.setPleuralDecompression(startVitalSigns.getChestTube());
        }
    }

    public AirwayAndBreathing(AirwayAndBreathing airwayAndBreathing) {
        this.airway=airwayAndBreathing.getAirway();
        this.percentageO2=airwayAndBreathing.getPercentageO2();
        this.tracheus=airwayAndBreathing.getTracheus();
        this.failedIOT=airwayAndBreathing.getFailedIOT();
        this.inhalation=airwayAndBreathing.getInhalation();
        this.pleuralDecompression=airwayAndBreathing.getPleuralDecompression();
    }

    public static int getNumberOfFields() {
        return 6;
    }

    public AirwayEnum getAirway() {
        return airway;
    }

    public Integer getPercentageO2() {
        return percentageO2;
    }

    public Boolean getTracheus() {
        return tracheus;
    }

    public Boolean getFailedIOT() {
        return failedIOT;
    }

    public Boolean getInhalation() {
        return inhalation;
    }

    public PleuralDecompressionEnum getPleuralDecompression() {
        return pleuralDecompression;
    }

    public static void computeNormalizationFactors() {
        AirwayAndBreathing.percentageO2Normalization[NormalizationEnum.MAX_MIN.ordinal()]=StatisticUtilities.computeDistance(
                AirwayAndBreathing.percentageO2MaxValue,AirwayAndBreathing.percentageO2MinValue);
        AirwayAndBreathing.percentageO2Normalization[NormalizationEnum.MEAN_VARIANCE.ordinal()]=
                StatisticUtilities.computeVariance(AirwayAndBreathing.percentageO2Values);
    }

    public static double getPercentageO2Normalization(NormalizationEnum normalizationFactor) {
        return AirwayAndBreathing.percentageO2Normalization[normalizationFactor.ordinal()];
    }

    public void setAirway(String airway) {
        this.airway = AirwayEnum.getValue(airway);
    }

    public void setPercentageO2(String percentageO2) {
        this.percentageO2 = Integer.parseInt(percentageO2);
    }

    public void setTracheus(String tracheus) {
        this.tracheus = tracheus.equals("Si");
    }

    public void setFailedIOT(String failedIOT) {
        this.failedIOT = failedIOT.equals("Si");
    }

    public void setInhalation(String inhalation) {
        this.inhalation = inhalation.equals("Si");
    }

    public void setPleuralDecompression(String pleuralDecompression) {
        this.pleuralDecompression = PleuralDecompressionEnum.getValue(pleuralDecompression);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AirwayAndBreathing)) return false;
        AirwayAndBreathing that = (AirwayAndBreathing) o;
        return getAirway() == that.getAirway() &&
                Objects.equals(getPercentageO2(), that.getPercentageO2()) &&
                Objects.equals(getTracheus(), that.getTracheus()) &&
                Objects.equals(getFailedIOT(), that.getFailedIOT()) &&
                Objects.equals(getInhalation(), that.getInhalation()) &&
                getPleuralDecompression() == that.getPleuralDecompression();
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        fields[0]=RNNUtilities.getDoubleFromEnum(this.airway);
        fields[1]=RNNUtilities.getDoubleFromInteger(this.percentageO2);
        fields[2]=RNNUtilities.getDoubleFromBoolean(this.tracheus);
        fields[3]=RNNUtilities.getDoubleFromBoolean(this.failedIOT);
        fields[4]=RNNUtilities.getDoubleFromBoolean(this.inhalation);
        fields[5]=RNNUtilities.getDoubleFromEnum(this.pleuralDecompression);
        return fields;
    }
}
