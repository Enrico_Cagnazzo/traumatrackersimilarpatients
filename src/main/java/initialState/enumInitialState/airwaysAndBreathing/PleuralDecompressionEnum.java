package initialState.enumInitialState.airwaysAndBreathing;

public enum PleuralDecompressionEnum {
    NON_PRESENTE,
    DESTRA,
    SINISTRA,
    BILATERALE;

    public static PleuralDecompressionEnum getValue(String chestTube) {
        switch (chestTube) {
            case "Non presente":
                return PleuralDecompressionEnum.NON_PRESENTE;
            case "Destra":
                return PleuralDecompressionEnum.DESTRA;
            case "Sinistra":
                return PleuralDecompressionEnum.SINISTRA;
            case "Bilaterale":
                return PleuralDecompressionEnum.BILATERALE;
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name();
    }

}