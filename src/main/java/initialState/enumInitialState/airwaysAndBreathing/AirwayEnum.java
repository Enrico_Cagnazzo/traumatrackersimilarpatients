package initialState.enumInitialState.airwaysAndBreathing;

public enum AirwayEnum {
    RESPIRO_SPONTANEO,
    PRESIDIO_SOVRAGLOTTICO,
    TUBO_TRACHEALE;

    @Override
    public String toString() {
        return this.name();
    }

    public static AirwayEnum getValue(String airway) {
        switch (airway) {
            case "Respiro Spontaneo":
                return AirwayEnum.RESPIRO_SPONTANEO;
            case "Presidio Sovraglottico":
                return AirwayEnum.PRESIDIO_SOVRAGLOTTICO;
            case "Tubo Tracheale": case "Tubo Tracheale (TT)":
                return AirwayEnum.TUBO_TRACHEALE;
        }
        return null;
    }

}