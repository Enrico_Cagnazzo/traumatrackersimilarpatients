package initialState.enumInitialState.neurologicalExamination;

public enum EyesDeviationsEnum {
    NON_PRESENTE,
    VERSO_DESTRA,
    VERSO_SINISTRA;

    public static EyesDeviationsEnum getValue(String eyesDeviation) {
        switch (eyesDeviation) {
            case "Non presente":
                return EyesDeviationsEnum.NON_PRESENTE;
            case "Verso Destra":
                return EyesDeviationsEnum.VERSO_DESTRA;
            case "Verso Sinistra":
                return EyesDeviationsEnum.VERSO_SINISTRA;
        }
        return null;
    }

    @Override
    public String toString() {
        return this.name();
    }

}