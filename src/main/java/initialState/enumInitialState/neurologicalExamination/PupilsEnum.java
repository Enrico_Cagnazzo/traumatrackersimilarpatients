package initialState.enumInitialState.neurologicalExamination;


public enum PupilsEnum{
    NORMALE,
    ANISO,
    MIDRIASI;

    @Override
    public String toString() {
        return this.name();
    }

    public static PupilsEnum getValue(String pupils) {
        switch (pupils) {
            case "Normale":
                return PupilsEnum.NORMALE;
            case "Aniso":
                return PupilsEnum.ANISO;
            case "Midriasi":
                return PupilsEnum.MIDRIASI;
        }
        return null;
    }

}