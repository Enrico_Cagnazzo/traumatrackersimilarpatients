package initialState.enumInitialState.neurologicalExamination;

public enum OtorrhagiaEnum {
    NON_PRESENTE,
    DESTRA,
    SINISTRA,
    BILATERALE;

    @Override
    public String toString() {
        return this.name();
    }

    public static OtorrhagiaEnum getValue(String earsBlood) {
        switch (earsBlood) {
            case "Non presente":
                return OtorrhagiaEnum.NON_PRESENTE;
            case "Destra":
                return OtorrhagiaEnum.DESTRA;
            case "Sinistra":
                return OtorrhagiaEnum.SINISTRA;
            case "Bilaterale":
                return OtorrhagiaEnum.BILATERALE;
        }
        return null;
    }


}