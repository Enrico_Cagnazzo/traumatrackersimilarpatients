package initialState.enumInitialState;

public enum BurnsEnum {
    NON_PRESENTE,
    TERMICA,
    CHIMICA;

    @Override
    public String toString() {
        return this.name();
    }

    public static BurnsEnum getValue(String burn) {
        switch (burn) {
            case "Non presente":
                return BurnsEnum.NON_PRESENTE;
            case "Termica":
                return BurnsEnum.TERMICA;
            case "Chimica":
                return BurnsEnum.CHIMICA;
        }
        return null;
    }

}