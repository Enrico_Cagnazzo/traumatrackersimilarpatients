package initialState.enumInitialState.vitalSigns;

public enum HeartRateEnum {
    NORMALE,
    TACHICARDICO,
    BRADICARDICO;

    @Override
    public String toString() {
        return this.name();
    }

    public static HeartRateEnum getValue(String value){
        switch (value){
            case "Normale":
                return HeartRateEnum.NORMALE;
            case "Tachicardico":
                return HeartRateEnum.TACHICARDICO;
            case "Bradicardico":
                return HeartRateEnum.BRADICARDICO;
        }
        return null;
    }

}