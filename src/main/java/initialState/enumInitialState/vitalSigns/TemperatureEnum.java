package initialState.enumInitialState.vitalSigns;

public enum TemperatureEnum {
    NORMOTERMICO,
    IPOTERMICO,
    IPERTERMICO;

    @Override
    public String toString() {
        return this.name();
    }

    public static TemperatureEnum getValue(String value){
        switch (value) {
            case "Normotermico":
                return TemperatureEnum.NORMOTERMICO;
            case "Ipotermico":
                return TemperatureEnum.IPOTERMICO;
            case "Ipertermico":
                return TemperatureEnum.IPERTERMICO;
        }
        return null;
    }

}