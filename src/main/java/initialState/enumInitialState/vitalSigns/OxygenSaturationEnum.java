package initialState.enumInitialState.vitalSigns;

public enum OxygenSaturationEnum {
    NORMALE,
    IPOSSICO;

    @Override
    public String toString() {
        return this.name();
    }

    public static OxygenSaturationEnum getValue(String value){
        switch (value){
            case "Normale":
                return OxygenSaturationEnum.NORMALE;
            case "Ipossico":
                return OxygenSaturationEnum.IPOSSICO;
        }
        return null;
    }

}