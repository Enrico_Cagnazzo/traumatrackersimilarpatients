package initialState.enumInitialState.vitalSigns;

public enum EndTidalCO2Enum {
    NORMALE,
    IPOCAPNICO,
    IPERCAPNICO;

    @Override
    public String toString() {
        return this.name();
    }

    public static EndTidalCO2Enum getValue(String endTidalCO2) {
        switch (endTidalCO2) {
            case "Normale":
                return EndTidalCO2Enum.NORMALE;
            case "Ipocapnico":
                return EndTidalCO2Enum.IPOCAPNICO;
            case "Ipercapnico":
                return EndTidalCO2Enum.IPERCAPNICO;
        }
        return null;
    }
}