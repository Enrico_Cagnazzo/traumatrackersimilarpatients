package initialState.enumInitialState.vitalSigns;

public enum SystolicBloodPressureEnum {
    NORMALE,
    IPOTESO,
    IPERTESO;

    @Override
    public String toString() {
        return this.name();
    }

    public static SystolicBloodPressureEnum getValue(String value){
        switch (value) {
            case "Normale":
                return SystolicBloodPressureEnum.NORMALE;
            case "Ipoteso":
                return SystolicBloodPressureEnum.IPOTESO;
            case "Iperteso":
                return SystolicBloodPressureEnum.IPERTESO;
        }
        return null;
    }

}