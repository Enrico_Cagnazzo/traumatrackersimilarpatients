package initialState;

import initialState.*;
import report.StartVitalSigns;

public class InitialState {
    private VitalSigns vitalSigns;
    private AirwayAndBreathing airwayAndBreathing;
    private NeurologicalExamination neurologicalExamination;
    private PeripheralWristsEvaluation peripheralWristsEvaluation;
    private VascularAccesses vascularAccesses;
    private OrthopaedicInjuries orthopaedicInjuries;
    private Burns burns;

    public InitialState(StartVitalSigns startVitalSigns) {
        this.vitalSigns=new VitalSigns(startVitalSigns);
        this.airwayAndBreathing=new AirwayAndBreathing(startVitalSigns);
        this.neurologicalExamination=new NeurologicalExamination(startVitalSigns);
        this.peripheralWristsEvaluation=new PeripheralWristsEvaluation(startVitalSigns);
        this.vascularAccesses=new VascularAccesses(startVitalSigns);
        this.orthopaedicInjuries=new OrthopaedicInjuries(startVitalSigns);
        this.burns=new Burns(startVitalSigns);
    }

    public VitalSigns getVitalSigns() {
        return vitalSigns;
    }

    public AirwayAndBreathing getAirwayAndBreathing() {
        return airwayAndBreathing;
    }

    public NeurologicalExamination getNeurologicalExamination() {
        return neurologicalExamination;
    }

    public PeripheralWristsEvaluation getPeripheralWristsEvaluation() {
        return peripheralWristsEvaluation;
    }

    public VascularAccesses getVascularAccesses() {
        return vascularAccesses;
    }

    public OrthopaedicInjuries getOrthopaedicInjuries() {
        return orthopaedicInjuries;
    }

    public Burns getBurns() {
        return burns;
    }


}
