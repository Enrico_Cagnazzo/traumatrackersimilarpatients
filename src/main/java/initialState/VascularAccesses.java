package initialState;

import similarPatientsCalculation.NormalizationEnum;
import utilities.RNNUtilities;
import utilities.StatisticUtilities;
import utilities.StringUtilities;
import report.StartVitalSigns;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class VascularAccesses {

    private static Integer numberOfPeripheralMinValue;
    private static Integer numberOfPeripheralMaxValue;
    private static Set<Number> numberOfPeripheralValues=new HashSet<>();
    private static double[] numberOfPeripheralNormalization = new double[NormalizationEnum.values().length];

    private Integer numberOfPeripheral;
    private Boolean intraosseus;
    private Boolean cvc;

    public VascularAccesses(StartVitalSigns startVitalSigns) {
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getPeripherals())){
            this.setNumberOfPeripheral(startVitalSigns.getPeripherals());
            VascularAccesses.numberOfPeripheralValues.add(this.numberOfPeripheral);
            if (VascularAccesses.numberOfPeripheralMinValue==null || this.numberOfPeripheral<VascularAccesses.numberOfPeripheralMinValue)
                VascularAccesses.numberOfPeripheralMinValue=this.numberOfPeripheral;
            if (VascularAccesses.numberOfPeripheralMaxValue==null || this.numberOfPeripheral>VascularAccesses.numberOfPeripheralMaxValue)
                VascularAccesses.numberOfPeripheralMaxValue=this.numberOfPeripheral;
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getIntraosseous())){
            this.setIntraosseus(startVitalSigns.getIntraosseous());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getCvc())){
            this.setCvc(startVitalSigns.getCvc());
        }
    }

    public VascularAccesses(VascularAccesses vascularAccesses) {
        this.numberOfPeripheral=vascularAccesses.getNumberOfPeripheral();
        this.intraosseus=vascularAccesses.getIntraosseus();
        this.cvc=vascularAccesses.getCvc();
    }

    public static int getNumberOfFields() {
        return 3;
    }

    public Integer getNumberOfPeripheral() {
        return numberOfPeripheral;
    }

    public Boolean getIntraosseus() {
        return intraosseus;
    }

    public Boolean getCvc() {
        return cvc;
    }

    public static void computeNormalizationFactors() {
        VascularAccesses.numberOfPeripheralNormalization[NormalizationEnum.MEAN_VARIANCE.ordinal()]=
                StatisticUtilities.computeVariance(VascularAccesses.numberOfPeripheralValues);
        VascularAccesses.numberOfPeripheralNormalization[NormalizationEnum.MAX_MIN.ordinal()]=StatisticUtilities.computeDistance(
                VascularAccesses.numberOfPeripheralMaxValue,VascularAccesses.numberOfPeripheralMinValue);
    }

    public static double getNumberOfPeripheralNormalization(NormalizationEnum normalizationFactor) {
        return VascularAccesses.numberOfPeripheralNormalization[normalizationFactor.ordinal()];
    }

    public void setNumberOfPeripheral(String numberOfPeripheral) {
        this.numberOfPeripheral = Integer.valueOf(numberOfPeripheral);
    }

    public void setIntraosseus(String intraosseus) {
        this.intraosseus = intraosseus.equals("Si");
    }

    public void setCvc(String cvc) {
        this.cvc = cvc.equals("Si");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VascularAccesses)) return false;
        VascularAccesses that = (VascularAccesses) o;
        return Objects.equals(getNumberOfPeripheral(), that.getNumberOfPeripheral()) &&
                Objects.equals(getIntraosseus(), that.getIntraosseus()) &&
                Objects.equals(getCvc(), that.getCvc());
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        fields[0]=RNNUtilities.getDoubleFromInteger(numberOfPeripheral);
        fields[1]=RNNUtilities.getDoubleFromBoolean(intraosseus);
        fields[2]=RNNUtilities.getDoubleFromBoolean(cvc);
        return fields;
    }
}
