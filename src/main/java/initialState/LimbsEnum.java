package initialState;

public enum LimbsEnum {
    UPPER_LEFT,
    UPPER_RIGHT,
    LOWER_LEFT,
    LOWER_RIGHT;

    @Override
    public String toString() {
        return this.name();
    }

}
