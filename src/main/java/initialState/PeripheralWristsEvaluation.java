package initialState;

import utilities.RNNUtilities;
import utilities.StringUtilities;
import report.StartVitalSigns;

import java.util.Arrays;

public class PeripheralWristsEvaluation {

    private Boolean[] wrists=new Boolean[4];

    public PeripheralWristsEvaluation(StartVitalSigns startVitalSigns) {
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getUpperLimbsLeftPeripheralWrists())){
            this.setWrist(LimbsEnum.UPPER_LEFT, startVitalSigns.getUpperLimbsLeftPeripheralWrists());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getUpperLimbsRightPeripheralWrists())){
            this.setWrist(LimbsEnum.UPPER_RIGHT, startVitalSigns.getUpperLimbsRightPeripheralWrists());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getLowerLimbsLeftPeripheralWrists())){
            this.setWrist(LimbsEnum.LOWER_LEFT, startVitalSigns.getLowerLimbsLeftPeripheralWrists());
        }
        if (!StringUtilities.isNullOrEmpty(startVitalSigns.getLowerLimbsRightPeripheralWrists())){
            this.setWrist(LimbsEnum.LOWER_RIGHT, startVitalSigns.getLowerLimbsRightPeripheralWrists());
        }
    }

    public PeripheralWristsEvaluation(PeripheralWristsEvaluation peripheralWristsEvaluation) {
        this.wrists=peripheralWristsEvaluation.getWrists();
    }

    public static int getNumberOfFields() {
        return 4;
    }

    public Boolean getSingleWrist(LimbsEnum limb) {
        return wrists[limb.ordinal()];
    }

    public Boolean[] getWrists() {
        return wrists.clone();
    }

    public void setWrist(LimbsEnum limb, String peripheralWrist) {
        this.wrists[limb.ordinal()] = peripheralWrist.equals("Si");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PeripheralWristsEvaluation)) return false;
        PeripheralWristsEvaluation that = (PeripheralWristsEvaluation) o;
        return Arrays.equals(wrists, that.wrists);
    }

    public double[] getFields() {
        double fields[]=new double[getNumberOfFields()];
        fields[0]=RNNUtilities.getDoubleFromBoolean(wrists[0]);
        fields[1]=RNNUtilities.getDoubleFromBoolean(wrists[1]);
        fields[2]=RNNUtilities.getDoubleFromBoolean(wrists[2]);
        fields[3]=RNNUtilities.getDoubleFromBoolean(wrists[3]);
        return fields;
    }
}
