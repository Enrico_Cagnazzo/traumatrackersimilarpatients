package utilities;

import initialState.*;

import java.util.Set;

public class StatisticUtilities {
    public final static double DIFFERENT_CATEGORICAL_DISTANCE=1;

    private static double computeMean(Set<Number> values){
        if (values.size()==0) return 0;
        double sum=0;
        for (Number n: values) {
            sum+=n.doubleValue();
        }
        return sum/values.size();
    }

    public static double computeVariance(Set<Number> values){
        double mean=computeMean(values);
        if (values.size()==0) return 0;
        double discrepancySum=0;
        for (Number n: values) {
            discrepancySum+=Math.pow(n.doubleValue()-mean,2);
        }
        return Math.sqrt(discrepancySum/values.size());
    }

    public static double getSquareDistance(Number aN, Number bN, double normalizationFactor){
        double a=aN.doubleValue(), b=bN.doubleValue();
        double fieldSquareDistance = (a - b) / normalizationFactor;
        fieldSquareDistance=Math.pow(fieldSquareDistance,2);
        return fieldSquareDistance;
    }

    public static double computeDistance(Integer maxValue, Integer minValue) {
        return maxValue!=null && minValue!=null ? maxValue-minValue : 0;
    }

    public static double computeDistance(Double maxValue, Double minValue) {
        return maxValue!=null && minValue!=null ? maxValue-minValue : 0;
    }

    public static void computeNormalizationFactors() {
        AirwayAndBreathing.computeNormalizationFactors();
        Burns.computeNormalizationFactors();
        NeurologicalExamination.computeNormalizationFactors();
        OrthopaedicInjuries.computeNormalizationFactors();
        VascularAccesses.computeNormalizationFactors();
        VitalSigns.computeNormalizationFactors();
    }
}
