package utilities;

import inputElaboration.Database;
import report.Patient;

public class TestUtilities {

    public static class PairPatient{
        private Patient patientA;
        private Patient patientB;

        public PairPatient(Patient patientA, Patient patientB) {
            this.patientA = patientA;
            this.patientB = patientB;
        }

        public Patient getPatientA() {
            return patientA;
        }

        public Patient getPatientB() {
            return patientB;
        }
    }

    public static PairPatient definePatients(){
        Patient patientA=null;
        Patient patientB=null;
        try{
            Database.create();
            for (Patient patient : Database.getInstance().getDatabase()) {
                if (patient.getId().equals("rep-20180126-000450")) {
                    patientA = patient;
                    if (patientB != null)
                        return new PairPatient(patientA,patientB);
                }
                if (patient.getId().equals("rep-20180208-225204")){
                    patientB = patient;
                    if (patientA != null)
                        return new PairPatient(patientA,patientB);
                }
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return new PairPatient(null,null);
    }

    public static Patient getShockedPatient() {
        for (Patient patient : Database.getInstance().getDatabase())
            if (patient.getId().equals("rep-20170709-195719"))
                return patient;
        return null;
    }

    public static Patient getPatientWithSuperflousEvents(){
        for (Patient patient : Database.getInstance().getDatabase())
            if (patient.getId().equals("rep-20180125-123513"))
                return patient;
        return null;
    }
}
