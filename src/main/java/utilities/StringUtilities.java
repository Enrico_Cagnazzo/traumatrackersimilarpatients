package utilities;

public class StringUtilities {

    public static boolean isNullOrEmpty(String s){
        return s==null || s.isEmpty();
    }

}
