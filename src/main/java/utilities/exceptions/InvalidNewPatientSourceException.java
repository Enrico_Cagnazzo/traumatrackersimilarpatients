package utilities.exceptions;

public class InvalidNewPatientSourceException extends Exception {
    @Override
    public String toString(){
        return "New patient report path empty or null";
    }
}
