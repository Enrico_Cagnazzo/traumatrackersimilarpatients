package utilities.exceptions;

public class InvalidReportFileException extends Exception {
    private String path;

    public InvalidReportFileException(String path) {
        this.path=path;
    }

    @Override
    public String toString(){
        return this.path+" is an invalid report file";
    }
}
