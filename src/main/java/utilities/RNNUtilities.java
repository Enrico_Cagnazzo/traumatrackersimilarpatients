package utilities;

public class RNNUtilities {
    private static double NULL_TO_DOUBLE=-100;
    
    public static double getDoubleFromString(String s){
        return StringUtilities.isNullOrEmpty(s)? RNNUtilities.NULL_TO_DOUBLE:Double.parseDouble(s);
    }

    public static double getDoubleFromEnum(Enum e) {
        return e==null?RNNUtilities.NULL_TO_DOUBLE:e.ordinal();
    }

    public static double getDoubleFromInteger(Integer i) {
        return i==null?RNNUtilities.NULL_TO_DOUBLE:i.doubleValue();
    }

    public static double getDoubleFromDouble(Double d) {
        return d==null?RNNUtilities.NULL_TO_DOUBLE: d;
    }

    public static double getDoubleFromBoolean(Boolean b) {
        return b==null?RNNUtilities.NULL_TO_DOUBLE: b ? 1 : 0;
    }

    public static double getHashDoubleFromString(String s) {
        return StringUtilities.isNullOrEmpty(s)? RNNUtilities.NULL_TO_DOUBLE:s.hashCode();
    }
}
