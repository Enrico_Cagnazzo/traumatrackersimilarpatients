package report;

public class Report {
    private String _id;
    private String _version;
    private String startOperatorId;
    private String startOperatorDescription;
    private String secondaryActivation;
    private String[] traumaTeamMembers;
    private String startDate;
    private String startTime;
    private String endDate;
    private String endTime;
    private Iss iss;
    private String finalDestination;
    private PatientInfo patientInfo;
    private Anamnesi anamnesi;
    private Preh preh;
    private StartVitalSigns startVitalSigns;
    private Event[] events;

    public Report() {
    }

    public String getId() {
        return _id;
    }

    public PatientInfo getPatientInfo() {
        return patientInfo;
    }

    public Iss getIss() {
        return iss;
    }

    public Anamnesi getAnamnesi() {
        return anamnesi;
    }

    public Preh getPreh() {
        return preh;
    }

    public StartVitalSigns getStartVitalSigns() {
        return startVitalSigns;
    }

    public Event[] getEvents() {
        return events;
    }
}