package report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Event {
    private String eventId;
    private String date;
    private String time;
    private String place;
    private String type;
    private EventContent content;

    public Event() {
    }

    public int getEventID() {
        return Integer.parseInt(eventId);
    }

    public Date getDateAndTime() {
        String dateString=date+" "+time;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getPlace() {
        return place;
    }

    public String getType() {
        return type;
    }

    public EventContent getContent() {
        return content;
    }
}
