package report;

public class StartVitalSigns {
    private String Temp;
    private String TempValue;
    private String HR;
    private String HRValue;
    private String BP;
    private String BPValue;
    private String SpO2;
    private String SpO2Value;
    private String EtCO2;
    private String EtCO2Value;
    private String ExtBleeding;
    private String Airway;
    private String Tracheo;
    private String Inhalation;
    private String IntubationFailed;
    private String ChestTube;
    private String OxygenPercentage;
    private String GCSTotal;
    private String GCSMotor;
    private String GCSVerbal;
    private String GCSEyes;
    private String Sedated;
    private String Pupils;
    private String EyesDeviation;
    private String EarsBlood;
    private String UpperLimbsRightMotility;
    private String UpperLimbsLeftMotility;
    private String LowerLimbsRightMotility;
    private String LowerLimbsLeftMotility;
    private String UpperLimbsRightSensitivity;
    private String UpperLimbsLeftSensitivity;
    private String LowerLimbsRightSensitivity;
    private String LowerLimbsLeftSensitivity;
    private String SphincterTone;
    private String Priapism;
    private String UpperLimbsRightPeripheralWrists;
    private String UpperLimbsLeftPeripheralWrists;
    private String LowerLimbsRightPeripheralWrists;
    private String LowerLimbsLeftPeripheralWrists;
    private String Peripherals;
    private String Intraosseous;
    private String Cvc;
    private String LimbsFracture;
    private String FractureExposition;
    private String FractureGustiloDegree;
    private String UnstableBasin;
    private String Burn;
    private String BurnDegree;
    private String BurnPercentage;

    public StartVitalSigns() {
    }

    public String getTemp() {
        return Temp;
    }

    public String getTempValue() {
        return TempValue;
    }

    public String getHR() {
        return HR;
    }

    public String getHRValue() {
        return HRValue;
    }

    public String getBP() {
        return BP;
    }

    public String getBPValue() {
        return BPValue;
    }

    public String getSpO2() {
        return SpO2;
    }

    public String getSpO2Value() {
        return SpO2Value;
    }

    public String getEtCO2() {
        return EtCO2;
    }

    public String getEtCO2Value() {
        return EtCO2Value;
    }

    public String getExtBleeding() {
        return ExtBleeding;
    }

    public String getAirway() {
        return Airway;
    }

    public String getTracheo() {
        return Tracheo;
    }

    public String getInhalation() {
        return Inhalation;
    }

    public String getIntubationFailed() {
        return IntubationFailed;
    }

    public String getChestTube() {
        return ChestTube;
    }

    public String getOxygenPercentage() {
        return OxygenPercentage;
    }

    public String getGCSTotal() {
        return GCSTotal;
    }

    public String getGCSMotor() {
        return GCSMotor;
    }

    public String getGCSVerbal() {
        return GCSVerbal;
    }

    public String getGCSEyes() {
        return GCSEyes;
    }

    public String getSedated() {
        return Sedated;
    }

    public String getPupils() {
        return Pupils;
    }

    public String getEyesDeviation() {
        return EyesDeviation;
    }

    public String getEarsBlood() {
        return EarsBlood;
    }

    public String getUpperLimbsRightMotility() {
        return UpperLimbsRightMotility;
    }

    public String getUpperLimbsLeftMotility() {
        return UpperLimbsLeftMotility;
    }

    public String getLowerLimbsRightMotility() {
        return LowerLimbsRightMotility;
    }

    public String getLowerLimbsLeftMotility() {
        return LowerLimbsLeftMotility;
    }

    public String getUpperLimbsRightSensitivity() {
        return UpperLimbsRightSensitivity;
    }

    public String getUpperLimbsLeftSensitivity() {
        return UpperLimbsLeftSensitivity;
    }

    public String getLowerLimbsRightSensitivity() {
        return LowerLimbsRightSensitivity;
    }

    public String getLowerLimbsLeftSensitivity() {
        return LowerLimbsLeftSensitivity;
    }

    public String getSphincterTone() {
        return SphincterTone;
    }

    public String getPriapism() {
        return Priapism;
    }

    public String getUpperLimbsRightPeripheralWrists() {
        return UpperLimbsRightPeripheralWrists;
    }

    public String getUpperLimbsLeftPeripheralWrists() {
        return UpperLimbsLeftPeripheralWrists;
    }

    public String getLowerLimbsRightPeripheralWrists() {
        return LowerLimbsRightPeripheralWrists;
    }

    public String getLowerLimbsLeftPeripheralWrists() {
        return LowerLimbsLeftPeripheralWrists;
    }

    public String getPeripherals() {
        return Peripherals;
    }

    public String getIntraosseous() {
        return Intraosseous;
    }

    public String getCvc() {
        return Cvc;
    }

    public String getLimbsFracture() {
        return LimbsFracture;
    }

    public String getFractureExposition() {
        return FractureExposition;
    }

    public String getFractureGustiloDegree() {
        return FractureGustiloDegree;
    }

    public String getUnstableBasin() {
        return UnstableBasin;
    }

    public String getBurn() {
        return Burn;
    }

    public String getBurnDegree() {
        return BurnDegree;
    }

    public String getBurnPercentage() {
        return BurnPercentage;
    }
}
