package report;

public class VitalSignsMon {
    private String temp;
    private String HR;
    private String DIA;
    private String SYS;
    private String SpO2;
    private String EtCO2;

    public Double getTemp() {
        return Double.valueOf(temp);
    }

    public Integer getHR() {
        return Integer.valueOf(HR);
    }

    public Integer getDIA() {
        return Integer.valueOf(DIA);
    }

    public Integer getSYS() {
        return Integer.valueOf(SYS);
    }

    public Integer getSpO2() {
        return Integer.valueOf(SpO2);
    }

    public Double getEtCO2() {
        return Double.valueOf(EtCO2);
    }

    public String getEtCO2String(){
        return EtCO2;
    }
}
