package report;

public class PatientInfo {
    private String code;
    private String sdo;
    private String erDeceased;
    private String admissionCode;
    private String gender;
    private String type;
    private String age;
    private String accidentDate;
    private String accidentTime;
    private String vehicle;
    private String fromOtherEmergency;
    private String otherEmergency;

    public String getGender() {
        return gender;
    }

    public String getAge() {
        return age;
    }
}
