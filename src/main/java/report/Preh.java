package report;

public class Preh {
    private String aValue;
    private String bPleuralDecompression;
    private String cBloodProtocol;
    private String cTpod;
    private String cPatientStationary;
    private String dGcsTotal;
    private String dAnisocoria;
    private String dMidriasi;
    private String eMotility;

    public Preh() {
    }

    public String getAValue() {
        return aValue;
    }
}
