package report;

import com.google.gson.Gson;
import org.bson.Document;
import shockPredictor.EventRNN;
import utilities.exceptions.InvalidReportFileException;
import report.Event;
import report.Report;
import initialState.InitialState;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Patient {
    private static int numShocked=0;

    private String id;
    private InitialState initialState;
    private Report report;

    private Event eventShock=null;
    private List<EventRNN> eventsForRNN=new ArrayList<>();

    public Patient(String reportPath) throws IOException, InvalidReportFileException {
        this.report=createReport(reportPath);
        if (this.report==null)
            throw new InvalidReportFileException(reportPath);
        extractInfoFromReport();
    }

    private Report createReport(String reportPath) throws IOException {
        Gson parser=new Gson();
        FileReader reportReader=new FileReader(reportPath);
        Report report= parser.fromJson(reportReader,Report.class);
        reportReader.close();
        return report;
    }

    public Patient(Document report) {
        this.report=createReport(report);
        extractInfoFromReport();
    }

    private Report createReport(Document reportDocument)  {
        Gson parser=new Gson();
        return parser.fromJson(reportDocument.toJson(),Report.class);
    }

    private void extractInfoFromReport() {
        this.id=this.report.getId();
        this.initialState=new InitialState(this.report.getStartVitalSigns());
        if (this.report.getEvents().length>0)
            defineEventsRNN();
    }

    private void defineEventsRNN() {
        Event[] events =this.report.getEvents();
        eventsForRNN.add(new EventRNN(events[0],this.initialState));
        for (int i = 1; i< events.length; i++) {
            eventsForRNN.add(new EventRNN(eventsForRNN.get(eventsForRNN.size()-1), events[i]));
            if (isShockStart(events[i])){
                this.eventShock=events[i];
                numShocked++;
                break;
            }
        }
        //vector squeeze
        for (int i =1; i<eventsForRNN.size(); i++){
            if (eventsForRNN.get(i).equals(eventsForRNN.get(i-1))){
                eventsForRNN.remove(i--);
            }
        }
    }

    private boolean isShockStart(Event event){
        return event.getType().equals("drug") && isShockDrug(event.getContent().getDrugId());
    }

    private boolean isShockDrug(String drugId) {
        return isAminaDrug(drugId) || drugId.equals("crystalloid") || drugId.equals("hypertonic")
                || drugId.equals("colloids") || drugId.equals("emazies");
    }

    private boolean isAminaDrug(String drugId) {
        return drugId.equals("adrenaline-continuous-inf")
                || drugId.equals("noradrenaline-continuous-inf")
                || drugId.equals("dopamine-continuous-inf");
    }

    public Event getEventShock() {
        return eventShock;
    }

    public List<EventRNN> getEventsForRNN() {
        return eventsForRNN;
    }

    public Report getReport() {
        return report;
    }

    public InitialState getInitialState() {
        return initialState;
    }

    public String getId() {
        return id;
    }

    public static int getNumShocked() {
        return numShocked;
    }
}
