package report;

public class Iss {
    private String totalIss;
    private HeadGroup headGroup;
    private FaceGroup faceGroup;
    private ToraxGroup toraxGroup;
    private AbdomenGroup abdomenGroup;
    private ExtremitiesGroup extremitiesGroup;
    private ExternaGroup externaGroup;

    public Iss() {
    }

    public String getTotalIss() {
        return totalIss;
    }

    public HeadGroup getHeadGroup() {
        return headGroup;
    }

    public FaceGroup getFaceGroup() {
        return faceGroup;
    }

    public ToraxGroup getToraxGroup() {
        return toraxGroup;
    }

    public AbdomenGroup getAbdomenGroup() {
        return abdomenGroup;
    }

    public ExtremitiesGroup getExtremitiesGroup() {
        return extremitiesGroup;
    }

    public ExternaGroup getExternaGroup() {
        return externaGroup;
    }

    public class HeadGroup {
        private String groupTotalIss;
        private String headNeckAis;
        private String brainAis;
        private String cervicalSpineAis;

        public HeadGroup() {
        }

        public String getGroupTotalIss() {
            return groupTotalIss;
        }

        public String getHeadNeckAis() {
            return headNeckAis;
        }

        public String getBrainAis() {
            return brainAis;
        }

        public String getCervicalSpineAis() {
            return cervicalSpineAis;
        }
    }

    public class FaceGroup {
        private String groupTotalIss;
        private String faceAis;

        public FaceGroup() {
        }
    }

    public class ToraxGroup {
        private String groupTotalIss;
        private String toraxAis;
        private String spineAis;

        public ToraxGroup() {
        }

    }

    public class AbdomenGroup {
        private String groupTotalIss;
        private String abdomenAis;
        private String lumbarSpineAis;

        public AbdomenGroup() {
        }

    }

    public class ExtremitiesGroup{
        private String groupTotalIss;
        private String upperExtremitiesAis;
        private String lowerExtremitiesAis;
        private String pelvicGirdleAis;

        public ExtremitiesGroup() {
        }

    }

    public class ExternaGroup {
        private String groupTotalIss;
        private String externaAis;

        public ExternaGroup() {
        }

    }

}
