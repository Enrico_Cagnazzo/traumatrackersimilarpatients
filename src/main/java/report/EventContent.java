package report;

public class EventContent {
    //se di tipo procedure
        private String procedureId;
        private String procedureDescription;
        private String procedureType;
        private String event;
        //in base a procedureId
        //intubation
            private String difficultAirway;
            private String inhalation;
            private String videolaringo;
            private String frova;
        //drainage || chest-tube
            private String right;
            private String left;
        //hemostasis
            private String epistat;
            private String suture;
            private String compression;
        //alr
            private String upper;
            private String lower;
    //se tipo diagnostic
        private String diagnosticId;
        private String diagnosticDescription;
        //in base a diagnosticId
        //skeleton-rx
            private String topLeft;
            private String topRight;
            private String bottomLeft;
            private String bottomRight;
        //abg
            private String lactates;
            private String be;
            private String ph;
            private String hb;
            private String glycemia;
            private String pco2;
            private String po2;
        //rotem
            private String fibtem;
            private String extem;
            private String hyperfibrinolysis;
    //se tipo drug
        private String drugId;
        private String drugDescription;
        private String administrationType;
        //private String event; già presente in procedure
        private String qty;
        private String unit;
        private String duration;
    //se tipo vital-sign
        private String name;
        private String value;
        private VitalSignsMon content;
    //se tipo text-note
        private String text;
    //se tipo room-in || room-out
        private String place;


    public String getProcedureId() {
        return procedureId;
    }

    public String getProcedureDescription() {
        return procedureDescription;
    }

    public String getProcedureType() {
        return procedureType;
    }

    public String getEvent() {
        return event;
    }

    public String getDifficultAirway() {
        return difficultAirway;
    }

    public String getInhalation() {
        return inhalation;
    }

    public String getVideolaringo() {
        return videolaringo;
    }

    public String getFrova() {
        return frova;
    }

    public String getRight() {
        return right;
    }

    public String getLeft() {
        return left;
    }

    public String getEpistat() {
        return epistat;
    }

    public String getSuture() {
        return suture;
    }

    public String getCompression() {
        return compression;
    }

    public String getUpper() {
        return upper;
    }

    public String getLower() {
        return lower;
    }

    public String getDiagnosticId() {
        return diagnosticId;
    }

    public String getDiagnosticDescription() {
        return diagnosticDescription;
    }

    public String getTopLeft() {
        return topLeft;
    }

    public String getTopRight() {
        return topRight;
    }

    public String getBottomLeft() {
        return bottomLeft;
    }

    public String getBottomRight() {
        return bottomRight;
    }

    public String getLactates() {
        return lactates;
    }

    public String getBe() {
        return be;
    }

    public String getPh() {
        return ph;
    }

    public String getHb() {
        return hb;
    }

    public String getGlycemia() {
        return glycemia;
    }

    public String getPco2() {
        return pco2;
    }

    public String getPo2() {
        return po2;
    }

    public String getFibtem() {
        return fibtem;
    }

    public String getExtem() {
        return extem;
    }

    public String getHyperfibrinolysis() {
        return hyperfibrinolysis;
    }

    public String getDrugId() {
        return drugId;
    }

    public String getDrugDescription() {
        return drugDescription;
    }

    public String getAdministrationType() {
        return administrationType;
    }

    public double getQty() {
        return Double.parseDouble(qty);
    }

    public String getUnit() {
        return unit;
    }

    public String getDuration() {
        return duration;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public VitalSignsMon getContent() {
        return content;
    }

    public String getText() {
        return text;
    }

    public String getPlace() {
        return place;
    }
}
