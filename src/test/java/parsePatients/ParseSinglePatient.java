package parsePatients;

import inputElaboration.NewPatient;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import initialState.LimbsEnum;
import report.Patient;
import utilities.exceptions.InvalidNewPatientSourceException;
import utilities.exceptions.InvalidReportFileException;
import initialState.enumInitialState.vitalSigns.TemperatureEnum;

import static org.junit.jupiter.api.Assertions.*;


public class ParseSinglePatient {
    private static Patient patient;

    @BeforeAll
    public static void createPatient(){
        try {
            patient=new Patient("C:\\Users\\Enrico\\Desktop\\Tesi magistrale\\materiale vario\\report-tt\\rep-20180130-142243.ttr");
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void sourceInvalidIsThrown(){
        Executable lambdaException = () -> NewPatient.setPath("");
        assertThrows(InvalidNewPatientSourceException.class,lambdaException);
    }

    @Test
    public void fileInvalidIsThrown(){
        Executable lambdaException = () -> NewPatient.setPath("C:\\Users\\Enrico\\Desktop\\Tesi magistrale\\materiale vario\\report-tt\\invalidFile.txt");
        assertThrows(InvalidReportFileException.class,lambdaException);
    }

    @Test
    public void correctIdFromReport(){
            assertEquals(patient.getReport().getId(),"rep-20180130-142243");
    }

    @Test
    public void correctFieldsFromReport(){
        assertEquals(patient.getReport().getIss().getTotalIss(),"12");
        assertEquals(patient.getReport().getIss().getHeadGroup().getCervicalSpineAis(),"0");
        assertEquals(patient.getReport().getPatientInfo().getGender(),"M");
        assertEquals(patient.getReport().getAnamnesi().getAnticoagulants(),"si");
        assertEquals(patient.getReport().getPreh().getAValue(),"SGA");
        assertEquals(patient.getReport().getStartVitalSigns().getTemp(),"Normotermico");
    }

    @Test
    public void correctFieldsFromInitialState(){
        assertEquals(patient.getInitialState().getVitalSigns().getTemperatureDiscrete(),TemperatureEnum.NORMOTERMICO);
        assertEquals(patient.getInitialState().getAirwayAndBreathing().getPercentageO2().intValue(),25);
        assertEquals(patient.getInitialState().getNeurologicalExamination().getVerbalGCS().intValue(),3);
        assertTrue(patient.getInitialState().getPeripheralWristsEvaluation().getSingleWrist(LimbsEnum.UPPER_LEFT));
        assertEquals(patient.getInitialState().getVascularAccesses().getNumberOfPeripheral().intValue(),25);
        assertEquals(patient.getInitialState().getOrthopaedicInjuries().getFractureGustiloDegree().intValue(),1);
        assertEquals(patient.getInitialState().getBurns().getBurnDegree().intValue(),1);
    }

    @Test
    public void correctFieldsEvents(){
        assertEquals(patient.getReport().getEvents()[0].getType(),"room-in");
        assertEquals(patient.getReport().getEvents()[0].getContent().getPlace(),"Shock-Room");

        assertEquals(patient.getReport().getEvents()[1].getEventID(),1);
        assertEquals(patient.getReport().getEvents()[1].getType(),"drug");
        assertEquals(patient.getReport().getEvents()[1].getContent().getDrugDescription(),"Cristalloidi");
        assertEquals(patient.getReport().getEvents()[1].getContent().getQty(),500);
    }
}
