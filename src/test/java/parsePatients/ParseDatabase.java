package parsePatients;

import inputElaboration.Database;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import similarPatientsCalculation.NormalizationEnum;
import utilities.StatisticUtilities;
import initialState.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParseDatabase {

    @BeforeAll
    public static void createDatabase(){
        Database.create();
        StatisticUtilities.computeNormalizationFactors();
    }

    @Test
    public void checkDatabaseDimension(){
        assertEquals(Database.getInstance().getDatabase().size(),281);
    }

    @Test
    public void checkMaxMinValues(){
        NormalizationEnum maxMin=NormalizationEnum.MAX_MIN;
        assertEquals(AirwayAndBreathing.getPercentageO2Normalization(maxMin),79);
        assertEquals(Burns.getBurnPercentageNormalization(maxMin),35);
        assertEquals(NeurologicalExamination.getTotalGCSNormalization(maxMin),15);
        assertEquals(OrthopaedicInjuries.getFractureGustiloDegreeNormalization(maxMin),2);
        assertEquals(VascularAccesses.getNumberOfPeripheralNormalization(maxMin),20);
        assertEquals(VitalSigns.getHeartRateNormalization(maxMin),102);
    }

    @Test
    public void checkMeanAndVarianceValues(){
        NormalizationEnum meanVariance=NormalizationEnum.MEAN_VARIANCE;
        assertEquals(Math.round(AirwayAndBreathing.getPercentageO2Normalization(meanVariance)),28);
        assertEquals(Math.round(Burns.getBurnPercentageNormalization(meanVariance)),14);
        assertEquals(Math.round(NeurologicalExamination.getTotalGCSNormalization(meanVariance)),5);
        assertEquals(Math.round(OrthopaedicInjuries.getFractureGustiloDegreeNormalization(meanVariance)),1);
        assertEquals(Math.round(VascularAccesses.getNumberOfPeripheralNormalization(meanVariance)),9);
        assertEquals(Math.round(VitalSigns.getHeartRateNormalization(meanVariance)),24);
    }
}
