package RNN;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import report.Patient;
import utilities.TestUtilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class Preprocessing {

    private static Patient shockedPatient;

    @BeforeAll
    public static void definePatients(){
        shockedPatient=TestUtilities.getShockedPatient();
    }
    @Test
    public void correctShockedPatient(){
        assertNotNull(shockedPatient);
        assertNotNull(shockedPatient.getEventShock());
    }

    @Test
    public void checkPreProcessing(){
        assertEquals(shockedPatient.getEventsForRNN().size(),7);
    }

    @Test
    public void checkSqueezeVector(){
        Patient patient=TestUtilities.getPatientWithSuperflousEvents();
        assertNotNull(patient);
        assertEquals(patient.getEventsForRNN().size(),1);
    }
}
