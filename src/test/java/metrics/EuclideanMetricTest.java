package metrics;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import similarPatientsCalculation.CalculateDistance;
import similarPatientsCalculation.NormalizationEnum;
import report.Patient;
import utilities.TestUtilities;


public class EuclideanMetricTest {
    private static Patient patientA;
    private static Patient patientB;
    private static CalculateDistance maxMin;
    private static CalculateDistance meanVariance;

    @BeforeAll
    public static void definePatients(){
        TestUtilities.PairPatient pairPatient = TestUtilities.definePatients();
        patientA=pairPatient.getPatientA();
        patientB=pairPatient.getPatientB();
    }

    @BeforeEach
    public void defineMetrics(){
        maxMin=new CalculateDistance(NormalizationEnum.MAX_MIN, new similarPatientsCalculation.EuclideanMetric());
        meanVariance=new CalculateDistance(NormalizationEnum.MEAN_VARIANCE, new similarPatientsCalculation.EuclideanMetric());
    }

    @Test
    public void compareSamePatientMaxMin(){
        Assertions.assertEquals(maxMin.computeDistance(patientA,patientA),0);
    }
    @Test
    public void compareSamePatientMeanVariance(){
        Assertions.assertEquals(meanVariance.computeDistance(patientA,patientA),0);
    }

    @Test
    public void compareDifferentPatientMaxMin() {
        Assertions.assertTrue(maxMin.computeDistance(patientA, patientB) > 0);
    }

    @Test
    public void compareDifferentPatientMeanVariance() {
        Assertions.assertTrue(meanVariance.computeDistance(patientA,patientB) > 0);
    }

    @Test
    public void symmetricMaxMinTest(){
        CalculateDistance maxMin2=new CalculateDistance(NormalizationEnum.MAX_MIN, new similarPatientsCalculation.EuclideanMetric());
        Assertions.assertEquals(maxMin.computeDistance(patientA,patientB),maxMin2.computeDistance(patientB,patientA));
    }

    @Test
    public void symmetricMeanVarianceTest(){
        CalculateDistance meanVariance2=new CalculateDistance(NormalizationEnum.MEAN_VARIANCE, new similarPatientsCalculation.EuclideanMetric());
        Assertions.assertEquals(meanVariance.computeDistance(patientA,patientB),
                meanVariance2.computeDistance(patientB,patientA));
    }

}
